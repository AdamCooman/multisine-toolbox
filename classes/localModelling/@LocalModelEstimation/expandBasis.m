function obj = expandBasis(obj)
% Generate the nD local polynomial basis functions for both the input basis
% as the transient basis

% Check whether order functions are instantiated
if ~isempty(obj.polynomial_order)
    generate_input = true; 
else
    generate_input = false;
end
if ~isempty(obj.transient_order)
    generate_transient = true;
else
    generate_transient = false;
end

% Check which basisses should be generated
if generate_input && generate_transient
    if obj.transient_order == obj.polynomial_order
        [obj.basis_input, obj.SymbolicG] = LocalModelEstimation.constructProductPolynomials(obj.basis, obj.polynomial_order);
        obj.basis_transient = obj.basis_input;
        obj.SymbolicT = obj.SymbolicG;
    else
        [obj.basis_input, obj.SymbolicG] = LocalModelEstimation.constructProductPolynomials(obj.basis, obj.polynomial_order);
        [obj.basis_transient, obj.SymbolicT] = LocalModelEstimation.constructProductPolynomials(obj.basis, obj.transient_order);
    end
elseif generate_input
    [obj.basis_input, obj.SymbolicG] = LocalModelEstimation.constructProductPolynomials(obj.basis, obj.polynomial_order);
elseif generate_transient
    [obj.basis_transient, obj.SymbolicT] = LocalModelEstimation.constructProductPolynomials(obj.basis, obj.transient_order);
end

end

