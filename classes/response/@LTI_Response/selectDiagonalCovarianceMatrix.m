function out = selectDiagonalCovarianceMatrix(~, A, m, n, loop)

if loop
    % Loop over the different frequencies
    out = zeros(m, n, size(A,3));
    
    % Select only the nonzero part
    nonzero = find(A(1,1,:));
    Nnz = length(nonzero);

    % Loop over the nonzero frequencies
    for ii = 1:Nnz
        out(:,:,nonzero(ii)) = reshape(diag(A(:,:,nonzero(ii))), [m,n]);
    end
else         
    out = reshape(diag(A), [m,n]);
end

end