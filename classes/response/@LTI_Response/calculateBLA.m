function obj = calculateBLA(obj, varargin)

%% PARSE THE INPUT
p = inputParser;
p.StructExpand = true;
p.FunctionName = 'calculateBLA';
p.PartialMatching = true;
p.addRequired('obj', @(x) isa(x,'LTI_Response'));
p.addParameter('method','ROBUST', @(x) any(strcmp(x,{'FAST','ROBUST'})));
p.parse(obj,varargin{:});
args = p.Results;

%% VALIDATE RESPONSE VALIDITY
obj.validateResponse();

%% CHECK WHICH METHOD IS CHOSEN AND EXECUTE IT

switch args.method
    case 'FAST'
        obj = obj.useFastMethod;
    case 'ROBUST'
        obj = obj.useRobustMethod;
end

end