function obj = times(obj, scalar)
% TIMES defines a multiplication of an object with a scalar.

if isa(obj, 'DataContainer')
    if isscalar(scalar) && isnumeric(scalar)
        % Check the size of the matrix
        obj.Data = cellfun(@(x) x.*scalar, obj.Data, 'uni', false);
    else
        error('Type of scalar was invalid.')
    end
elseif isa(obj, 'MultisineSignal')
    obj.MultisineContainer = feval(mfilename,obj.MultisineContainer,scalar);
elseif isa(scalar, 'DataContainer') || isa(scalar, 'MultisineSignal')
    obj = times(scalar, obj);
else
    error([mfilename ' functionality was not yet implemented for this class.']);
end

end