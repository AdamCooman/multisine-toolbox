function obj = uminus(obj)
% UMINUS puts a minus in front of all data.

if isa(obj, 'DataContainer')
        obj.Data = cellfun(@(x) -x, obj.Data, 'uni', false);
elseif isa(obj, 'MultisineSignal')
    obj.MultisineContainer = feval(mfilename,obj.MultisineContainer,scalar);
else
    error([mfilename ' functionality was not yet implemented for this class.']);
end

end