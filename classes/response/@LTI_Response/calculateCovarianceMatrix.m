function CvecBLA = calculateCovarianceMatrix(~, BLA, mZ, CvecZ)

% We use the first part of expression (106) to calculate CvecG
% We apply this function to the contents of each cell

% Local variables
Ny = size(BLA, 1);
Nu = size(BLA, 2);
F = size(BLA, 3);
CvecBLA = zeros(Ny*Nu, Ny*Nu, F);

% Select only the nonzero part
nonzero = find(BLA(1,1,:));
Nnz = length(nonzero);
BLA = full(BLA(:,:,nonzero));
mZ = full(mZ(:,:,nonzero));
CvecZ = full(CvecZ(:,:,nonzero));

% Loop over the nonzero frequencies
for i_freq = 1:Nnz
    Vt = [eye(Ny) -BLA(:,:,i_freq)];
    Ut = pinv(mZ(Ny+1:end,:,i_freq));
    T = kron(Ut.', Vt);
    CvecBLA(:,:,nonzero(i_freq)) = T * CvecZ(:,:,i_freq) * T';
end

end