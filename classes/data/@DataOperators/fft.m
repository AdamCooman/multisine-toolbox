function obj = fft(obj)
% Take the IFFT of a DataContainer, MultisineSignal or 
% MultisineResponse object.

if isa(obj,'DataContainer')
    if strcmp(obj.Domain,'TIME')
        % Setter does the fft.
        obj.Domain = 'FREQUENCY';
    else
        warning('DataContainer was already in the frequency domain, ignoring call.');
    end
elseif isa(obj,'MultisineSignal')
    obj.MultisineContainer = feval(mfilename,obj.MultisineContainer);
else
    error([mfilename ' functionality was not yet implemented for this class.']);
end

end