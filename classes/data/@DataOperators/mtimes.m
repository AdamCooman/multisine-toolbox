function obj = mtimes(matrix, obj)
% MTIMES defines a multiplication of an object with a matrix.
% WARNING: Only left multiplication with a matrix is currently permitted.

if isa(obj, 'DataContainer')
    if ismatrix(matrix) && isnumeric(matrix)
        % Check the size of the matrix
        [~,number_of_inputs] = size(matrix);
        if size(obj.Data{1},1) == number_of_inputs
            %tic
            obj.Data = cellfun(@(x) mtimesx(matrix,x) , obj.Data, 'uni', false);
            %toc
        else
            error('Column dimension of the matrix was incompatible with data.')
        end
    elseif isnumeric(matrix)
        % Check the size of the matrix
        [~,number_of_inputs,F] = size(matrix);
        if size(obj.Data{1},1) == number_of_inputs && size(obj.Data{1},3) == F
            %tic
            obj.Data = cellfun(@(x) mtimesx(matrix,x) , obj.Data, 'uni', false);
            %toc
        else
            error('Dimensions of the matrix were incompatible with data.')
        end
    elseif isa(matrix, 'DataContainer')
        obj.Data = cellfun(@(x,y) mtimesx(x, y), matrix.Data, obj.Data, 'uni', false);
    else
        error('No valid matrix was found.')
    end
elseif isa(obj, 'MultisineSignal')
    obj.MultisineContainer = feval(mfilename,matrix,obj.MultisineContainer);
elseif isa(matrix, 'DataContainer') || isa(matrix, 'MultisineSignal')
    error('Only left multiplication with a matrix is currently permitted.');
else
    error([mfilename ' functionality was not yet implemented for this class.']);
end
end

function C = mtimesx(A,B)
Nexp = size(B,2);
[Ny,~,F] = size(A);

if F == 1 
    nonzero = find(B(1,1,:));
    Nnz = length(nonzero);
    res = zeros([Ny,Nexp,Nnz]);
    dummyB = full(B(:,:,nonzero));
    for idx = 1:size(dummyB,3)
        res(:,:,idx) = A*dummyB(:,:,idx);
    end
    C = ndSparse(zeros(Ny,Nexp,size(B,3)));
    C(:,:,nonzero) = res;
    
else
    res = zeros([Ny,Nexp,F]);
    for idx = 1:F
        res(:,:,idx) = A(:,:,idx)*B(:,:,idx);
    end
    C = ndSparse(res);
end
end
