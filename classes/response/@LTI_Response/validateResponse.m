function res = validateResponse(obj)

%% CHECK THE INPUT ARGUMENTS.

p = inputParser();
p.addRequired('obj',@(x) isa(x,'LTI_Response'))
p.parse(obj);

%% CHECK IF ESTIMATING A BLA IS EVEN POSSIBLE

if isempty(obj.output)
    error('Output data is required to estimate the BLA.');
end

if isempty(obj.input) && isempty(obj.reference)
    error('Either input or reference data is required for estimation purposes.');
end

%% CHECK IF SIZES ARE CONSISTENT WITH THE SETTINGS

Nu = obj.NumberOfInputs;
Ny = obj.NumberOfOutputs;
Ne = obj.NumberOfExperiments;
R = obj.NumberOfRealisations;
P = obj.NumberOfPeriods;
N = obj.getDerivedSettings.N;

% output checking
outer_check = any(size(obj.output.Data) ~= [R,floor(P)]);
inner_check = cellfun(@(x) size(x)~=[Ny,Ne,N], obj.output.Data, 'uni', false);
if any([inner_check{:}]) || outer_check
    error('Dimensions of the output were inconsistent with the DataSettings.')
end

if ~isempty(obj.input)
    % input checking
    outer_check = any(size(obj.input.Data) ~= [R,floor(P)]);
    inner_check = cellfun(@(x) size(x)~=[Nu,Ne,N], obj.input.Data, 'uni', false);
    if any([inner_check{:}]) || outer_check
        error('Dimensions of the input were inconsistent with the DataSettings.')
    end
end

if ~isempty(obj.reference)
    % reference checking
    outer_check = any(size(obj.reference.Data) ~= [R,floor(P)]);
    inner_check = cellfun(@(x) size(x)~=[Nu,Ne,N], obj.reference.Data, 'uni', false);
    if any([inner_check{:}]) || outer_check
        error('Dimensions of the reference were inconsistent with the DataSettings.')
    end
end

%% RETURN TRUE
res = true;

end