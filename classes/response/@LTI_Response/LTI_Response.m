classdef LTI_Response < DataSettings & DataOperators
    % LTI_Response uses the Reference, Input and Output MultisineSignal to estimate the Best
    % Linear Approximation
    %
    % SIGNAL Properties:
    %    Reference      - Reference signal (MULTISINESIGNAL)
    %    Input          - Input signal (MULTISINESIGNAL)
    %    Output         - Output signal (MULTISINESIGNAL)
    %
    % Methods:
    %    MultisineResponse     - Class constructer that creates a MSResponse object
    %
    
    properties (Dependent) %CamelCase
        Reference
        Input
        Output
        ExcitedBins
    end
    
    properties (Access = protected) %snake_case
        reference
        input
        output
        excited_bins
    end
    
    % Internal variables which contain the results of the BLA analysis
    properties (SetAccess = private, GetAccess = public)
        U
        Y
        Z
        BLA
        CovarianceU
        CovarianceY
        CovarianceZ
        CovarianceBLA
        ModelInterpolationT
        ModelInterpolationG
    end
    
    % Constants
    properties (Constant, Access=protected)
        METHOD_LIST = {'FAST','ROBUST'}
    end
    
    %% CONSTRUCTOR
    methods
        
        function obj = LTI_Response(settingsInst,varargin)
            if  nargin ~= 0
                p = inputParser;
                p.StructExpand = true;
                p.FunctionName = 'LTI_Response';
                p.PartialMatching = true;
                p.addRequired('settingsInst'    , @(x) isa(x,'DataSettings'));
                p.addParameter('reference'      ,[], @(x) isa(x,'DataContainer') || isa(x,'MultisineSignal') || iscell(x));
                p.addParameter('input'          ,[], @(x) isa(x,'DataContainer') || isa(x,'MultisineSignal') || iscell(x));
                p.addParameter('output'         ,[], @(x) isa(x,'DataContainer') || isa(x,'MultisineSignal') || iscell(x));
                p.addParameter('excitedBins'          ,[], @(x) isvector(x));
                p.parse(settingsInst, varargin{:});
                args = p.Results;
                
                obj = obj.copyDataSettings(args.settingsInst,obj);
                
                % Assign variables (check the data consistency with the multisine_signal!)
                obj.Reference = args.reference;
                obj.Input = args.input;
                obj.Output = args.output;
                obj.excited_bins          = args.excitedBins;
            end
        end
    end
    
    %% OTHER METHODS
    
    methods
        
        function obj = calculateExcitedBins(obj,tol)
            % TODO: Should be a way to set this tolerance somewhere?
            if nargin < 2
                tol = -200;
            end
            
            % Either input or reference is given..we prefer reference of
            % course!
            if ~isempty(obj.reference)
                dummyContainer = obj.reference;
            else
                dummyContainer = obj.input;
            end
            if strcmp(dummyContainer.Domain, 'TIME')
                dummyContainer = fft(dummyContainer);
            end
            obj.excited_bins = find(db(full(dummyContainer.Data{1,1}(1,1,:)))>tol);
        end
        
    end
    
    %% GETTERS AND SETTERS
    methods
        
        % GET and SET Input
        function reference = get.Reference(obj)
            reference = obj.reference;
        end
        
        function obj = set.Reference(obj, reference)
            if ~LTI_Response.check_SignalConsistency(obj,reference)
                error('Reference data was not consistent.');
            end
            obj.reference = LTI_Response.modifySignal(obj,reference);
        end
        
        % GET and SET Input
        function input = get.Input(obj)
            input = obj.input;
        end
        
        function obj = set.Input(obj, input)
            if ~LTI_Response.check_SignalConsistency(obj,input)
                error('Input data was not consistent.');
            end
            obj.input = LTI_Response.modifySignal(obj,input);
        end
        
        % GET and SET Output
        function output = get.Output(obj)
            output = obj.output;
        end
        
        function obj = set.Output(obj, output)
            if ~LTI_Response.check_SignalConsistency(obj,output)
                error('Output data was not consistent.');
            end
            obj.output = LTI_Response.modifySignal(obj,output);
        end
        
        % GET and SET ExcitedBins
        function excited_bins = get.ExcitedBins(obj)
            excited_bins = obj.excited_bins;
        end
        
        function obj = set.ExcitedBins(obj, excited_bins)
            obj.excited_bins = excited_bins;
        end
    end
    
    %% CHECKERS
    methods (Static , Access = protected)
        
        function res = check_SignalConsistency(obj, signal)
            if ~isempty(signal)
%                 N = obj.getDerivedSettings.N;
%                 M = obj.NumberOfRealisations;
%                 P = obj.NumberOfPeriods;
%                 period_floor = floor(P);
%                 Ne = obj.NumberOfExperiments;
%                 
                switch class(signal)
                    case 'DataContainer'
                        data = signal.Data;
                    case 'MultisineSignal'
                        data = signal.MultisineContainer.Data;
                    case 'cell'
                        data = signal;
                end
%                 checkExternalSize = ~any(isequal(size(data),[M,period_floor]) || isequal(size(data), [M,1]));
%                 checkInternalSize = cellfun(@(x) size(x,2) == Ne && (size(x,3) == N || size(x,3) == N*P),data,'uni',false);
%                 if checkExternalSize || ~any([checkInternalSize{:}])
%                     error('Dimensions of the input were inconsistent with the DataSettings.')
%                 end
            end
            res = true;
        end
        
        function data_container = modifySignal(obj, signal)
            P = obj.NumberOfPeriods;
            
            if ~isempty(signal)
                switch class(signal)
                    case 'DataContainer'
                        data_container = signal;
%                         if ~data_container.expanded
%                             data_container = data_container.expandPeriods(P);
%                         end
                    case 'MultisineSignal'
                        data_container = signal.MultisineContainer;
%                         if ~data_container.expanded
%                             data_container = data_container.expandPeriods(P);
%                         end
                    case 'cell'
                        data_container = DataContainer(signal,obj.getDataSettings(obj),'split_periods', true);
                end
                % Convert the signal to the FREQUENCY domain
                data_container.Domain = 'FREQUENCY';
            else
                data_container = [];
            end
        end
        
        function obj1 =  stackZ(obj1, obj2)
            obj1 = obj1.run(@(x,y) [x;y], obj2);
        end

        function [obj1, obj2] =  splitZ(obj1, Ny)
            obj2 = obj1;
            obj1 = obj1.run(@(x) x(1:Ny,:,:));
            obj2 = obj2.run(@(x) x(Ny+1:end,:,:));
        end
        
        function [obj1, obj2] =  splitCovarianceZ(obj1, Ny, Nu)
            obj2 = obj1;
            obj1 = obj1.run(@(x) x(1:Ny,1:Ny,:));
            obj2 = obj2.run(@(x) x(end-Nu+1:end,end-Nu+1:end,:));
        end
        
        function C_stochastic = deriveStochasticDistortions(C_nonlinear, C_noise)
            C_stochastic = C_nonlinear - C_noise;
            
            indici = C_nonlinear.run(@(x,y) (abs(x) - abs(y)) < 0, C_noise);
            C_stochastic = C_stochastic.run(@(x,y) LTI_Response.putMatrixIndiciToZero(x,y), indici);
        end
        
        function x = putMatrixIndiciToZero(x, y)
            x(y) = 0;
        end
        
        function CYS = deriveOutputStochasticDistortions(CZS, G, Ny)
            F = size(CZS, 3);
            CYS = zeros(Ny, Ny, F);

            nonzero = find(G(1,1,:));
            Nnz = length(nonzero);
            res = zeros([Ny,Ny,Nnz]);
            dummyCZS = full(CZS(:,:,nonzero));
            dummyG = full(G(:,:,nonzero));
            for idx = 1:numel(nonzero)
                res(:,:,idx) = [eye(Ny), -dummyG(:,:,idx)] * dummyCZS(:,:,idx) * [eye(Ny); -dummyG(:,:,idx)'];
            end
            CYS(:,:,nonzero) = res;
        end
    end
end