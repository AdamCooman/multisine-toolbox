function obj = diag(obj)
% Take the DIAGONAL elements of the DataContainer object.

if isa(obj,'DataContainer')
    obj = obj.run(@(x) diagx(x));
elseif isa(obj,'MultisineSignal')
    obj.MultisineContainer = feval(mfilename,obj.MultisineContainer);
else
    error([mfilename ' functionality was not yet implemented for this class.']);
end

end

function B = diagx(A)
    B = zeros(size(A, 2), 1, size(A, 3));
    for ii = 1:size(A, 3)
        B(:,1,ii) = diag(A(:,:,ii));
    end
end
