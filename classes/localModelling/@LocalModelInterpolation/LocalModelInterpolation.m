classdef LocalModelInterpolation
    
    properties (Dependent) % CamelCase
        ModelCoefficients
        Symbolic
        GridValues
        ScalingGrid
        InterpolationValues
    end
    
    properties (GetAccess = public, SetAccess = protected)
        InterpolatedModel
    end
    
    properties (Access = protected) % snake_case
        model_coefficients
        symbolic
        grid_values
        scaling_grid
        interpolation_values
    end
    
    %% CONSTRUCTOR
    methods
        
        function obj = LocalModelInterpolation(varargin)
            p = inputParser;
            p.StructExpand = true;
            p.FunctionName = 'LocalModelInterpolation';
            p.PartialMatching = true;
            p.addParameter('modelCoefficients', [], @(x) iscell(x));
            p.addParameter('symbolic', [], @(x) isvector(x));
            p.addParameter('gridValues', [], @(x) iscell(x));
            p.addParameter('scalingGrid', [], @(x) iscell(x));
            p.addParameter('interpolationValues', [], @(x) iscell(x));
            p.parse(varargin{:});
            args = p.Results;
            
            obj.model_coefficients = args.modelCoefficients;
            obj.symbolic = args.symbolic;
            obj.grid_values = args.gridValues;
            obj.scaling_grid = args.scalingGrid;
            obj.interpolation_values = args.interpolationValues;
        end
        
    end
    
    %% OTHER METHODS
    
    methods (Static, Access = protected)
        
       
    end
    
    %% GETTERS AND SETTERS
    
    methods
        
        % GET and SET ModelCoefficients
        function model_coefficients = get.ModelCoefficients(obj)
            model_coefficients = obj.model_coefficients;
        end
        
        function obj = set.ModelCoefficients(obj, model_coefficients)
            if ~iscell(model_coefficients)
                error('ModelCoefficients is not a cell.');
            end
            obj.model_coefficients = model_coefficients;
        end
        
        % GET and SET Symbolic
        function symbolic = get.Symbolic(obj)
            symbolic = obj.symbolic;
        end
        
        function obj = set.Symbolic(obj, symbolic)
            if ~isvector(symbolic)
                error('Symbolic is not a vector.');
            end
            obj.symbolic = symbolic;
        end
        
        % GET and SET GridValues
        function grid_values = get.GridValues(obj)
            grid_values = obj.grid_values;
        end
        
        function obj = set.GridValues(obj, grid_values)
            if ~iscell(grid_values)
                error('GridValues is not a cell.');
            end
            obj.grid_values = grid_values;
        end
        
        % GET and SET ScalingGrid
        function scaling_grid = get.ScalingGrid(obj)
            scaling_grid = obj.scaling_grid;
        end
        
        function obj = set.ScalingGrid(obj, scaling_grid)
            if ~iscell(scaling_grid)
                error('ScalingGrid is not a cell.');
            end
            obj.scaling_grid = scaling_grid;
        end
        
        % GET and SET InterpolationValues
        function interpolation_values = get.InterpolationValues(obj)
            interpolation_values = obj.interpolation_values;
        end
        
        function obj = set.InterpolationValues(obj, interpolation_values)
            if ~iscell(interpolation_values)
                error('InterpolationValues is not a cell.');
            end
            obj.interpolation_values = interpolation_values;
        end
        
    end
    
    %% CHECKERS
    
    methods (Static , Access = protected)
       
    end
   
end