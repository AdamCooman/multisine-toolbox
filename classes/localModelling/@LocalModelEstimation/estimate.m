function obj = estimate(obj)
% Provide an estimate for the following quantitities:
%   - the FRF G
%   - the transient T
%   - the output noise covariance CY
%   - the covariance matrix of vec(G)

% Implode the output
obj.output = LocalModelEstimation.implodeCellToMatrix(obj.basis, obj.output);

% Estimate of the model coefficients
[obj.Theta, U, inv_sigma, V] = cellfun(@(x,y) LocalModelEstimation.computeTheta(x,y), obj.output, obj.jacobian, 'uni', false);

% Derive the FRF G 
if any(obj.polynomial_order)
    obj.G = cellfun(@(x) x(:, 1:obj.nu), obj.Theta, 'uni', false);
    obj.ModelCoefficientsG = cellfun(@(x) x(:, 1:size(obj.basis_input,2)*obj.nu), obj.Theta, 'uni', false);
end

% Derive the transient T is estimated
if any(obj.transient_order)
    obj.T = cellfun(@(x) x(:, end-size(obj.basis_transient,2)+1), obj.Theta, 'uni', false);
    obj.ModelCoefficientsT = cellfun(@(x) x(:, end-size(obj.basis_transient,2)+1:end), obj.Theta, 'uni', false);
end

% Derive the total degrees of freedom
dof = size(obj.jacobian{1},2) - size(obj.jacobian{1},1);

% Derive the covariance matrices CYNoise and CvecG
if ~isempty(obj.output_noise_variance)
    [obj.Y, obj.CYNoise, obj.CYMean, obj.CvecG, obj.CT, obj.CvecGNoise] = cellfun(@(r, s, t, u, v) LocalModelEstimation.deriveCovariances(r,s,t,u,v,obj.center_location,dof,obj.nu,size(obj.basis_input,2),size(obj.basis_transient,2)), obj.output, U, inv_sigma, V, obj.output_noise_variance, 'uni', false);
else
    [obj.Y, obj.CYNoise, obj.CYMean, obj.CvecG, obj.CT, obj.CvecGNoise] = cellfun(@(r, s, t, u) LocalModelEstimation.deriveCovariances(r,s,t,u,[],obj.center_location,dof,obj.nu,size(obj.basis_input,2),size(obj.basis_transient,2)), obj.output, U, inv_sigma, V, 'uni', false);
end

% For further characterisation, if needed
obj.U = U;
obj.InverseEigenValues = inv_sigma;
obj.V = V;

end

