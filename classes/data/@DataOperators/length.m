function res = length(obj)
% Extract the LENGTH of the internal matrices of the DataContainer.

if isa(obj,'DataContainer')
    aux = obj.Data{1,1};
    res = size(aux,3);
elseif isa(obj,'MultisineSignal')
    obj.MultisineContainer = feval(mfilename,obj.MultisineContainer);
else
    error([mfilename ' functionality was not yet implemented for this class.']);
end

end
