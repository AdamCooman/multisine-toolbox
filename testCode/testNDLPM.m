% Code to test the nD-LPM local model estimation

clc
close all
clear variables

%% GENERAL SETTINGS

fs = 1e3; % Sampling frequency
P  = 1;   % Number of periods

%% GENERATE ALL SETTINGS.

settingsInst = DataSettings();
settingsInst.FrequencySettings.fs = fs;
settingsInst.FrequencySettings.fres = 0.1;
settingsInst.NumberOfPeriods = P;
settingsInst.NumberOfInputs = 1;
settingsInst.NumberOfOutputs = 1;
settingsInst.NumberOfRealisations = 1;
settingsInst.NumberOfExperiments = 1;

%% GENERATE DIFFERENT POWER LEVELS

msConfigInst = MultisineConfig(settingsInst);
msConfigInst.GridMode = 'FULL';
msConfigInst.GridSettings.bandwidth = 100;
msConfigInst.AmplitudeMode = 'AMPLITUDE';
DerivedSettings = settingsInst.getDerivedSettings();
N = DerivedSettings.N;

powerLevels = 10;
cell_input = cell(1,P,powerLevels);
cell_output = cell(1,P,powerLevels);
plotG = zeros(1000, powerLevels);
for pp = 1:powerLevels
    msConfigInst.VoltageSettings.bin_voltage = {-20 + pp};
    ms_reference = msConfigInst.generateMultisine();
    ms_reference = ms_reference.MultisineContainer.selectExcitedBins(N/2+2:N/2+1001);
    
    % Generate power-dependent transfer function
    w0 = 2*pi*20+2*pi*pp;
    damping = 2/(1.5*pp);
    H = tf(w0^2, [1 2*damping*w0 w0^2]);
    G = freqresp(H, 2*pi*ms_reference.DomainValues);
    ms_output = G*ms_reference;
    
    plotG(:, pp) = G;
    
    cell_input(1,:,pp) = ms_reference.Data;
    cell_output(1,:,pp) = ms_output.Data;
end

% Add transient
% cell_output = cellfun(@(x) full(x) + 1.5, cell_output, 'uni', false);

% Put the data into data containers
data_input = ms_reference;
data_output = ms_output;
data_input.Data = cell_input;
data_output.Data = cell_output;

% Derive power values
rmsValues = data_input.run(@(x) rms(x));
rmsValues = rmsValues.run(@(x) x(1));
powerValues = zeros(1, powerLevels);
for ii = 1:powerLevels
    powerValues(ii) = full(rmsValues.Data{ii});    
end

% Pass the signals through a saturation nonlinearity
% data_output = data_output.run(@(x) tanh(x/2));

%% ESTIMATE A 2D LOCAL MODEL

lm = LocalModelEstimation();
lm.Basis = {data_input.DomainValues , 0.5*(1:powerLevels)};
lm.Input = data_input;
lm.Output = data_output;
lm.PolynomialOrder = [1 1];
lm.TransientOrder = [1 1];

lm = lm.expandBasis;
lm = lm.buildJacobian;
lm = lm.estimate;

%% Perform local modelling

% Perform the identification on the excited lines
lmConfig = LocalModelConfig();
lmConfig.LocalModelOrder = [4 2];
lmConfig.LocalTransientOrder = [4 2];
lmConfig.ExcitedBins = {1:1000, 1:powerLevels};
lmConfig.ExternalVariables = 1:powerLevels;
lmConfig.LocalWindowWidth = [5 3];
lm = LocalModelling(settingsInst, lmConfig, 'out', data_output, 'in', data_input);
lm = lm.run();

%% Plot results

plotGLPM = zeros(1000, powerLevels);
plotCGLPM = zeros(1000, powerLevels);
for pp = 1:powerLevels
    plotGLPM(:, pp) = lm.G.Data{1,1,pp};
    plotCGLPM(:, pp) = sqrt(lm.CvecG.Data{1,1,pp});
end

% Plot 2D transfer function 
figure
mesh(10*log10(powerValues), (data_input.DomainValues(:)), db(plotGLPM))
hold on
mesh(10*log10(powerValues), (data_input.DomainValues(:)), db((plotGLPM-plotG)./plotG))
