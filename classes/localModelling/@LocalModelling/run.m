function obj = run(obj)

    % Local variables
    Nr = obj.number_of_inputs;
    Nz = obj.number_of_outputs;
    Ne = obj.number_of_experiments;
    Ftot = numel(obj.excited_bins{1});
    
    % Derive the degrees of freedom starting from the LocalWindowWidth
    [dof, parameters_model, parameters_transient] = obj.calculateDOF();
    disp(['The Degrees of Freedom are equal to ', num2str(dof), '.']);
    
    % Extract the basis values corresponding with the excited and adjacent
    % points
    basis_values_excited{1} = obj.input.DomainValues(obj.excited_bins{1});
    if ~isempty(obj.adjacent_bins)
        basis_values_adjacent{1} = obj.input.DomainValues(obj.adjacent_bins{1});
    end
    if numel(obj.excited_bins) > 1
        basis_values_excited = [basis_values_excited, cellfun(@(x,y) x(y).', obj.external_variables, obj.excited_bins(2:end), 'uni', false)];
        if ~isempty(obj.adjacent_bins)
            basis_values_adjacent = [basis_values_adjacent, cellfun(@(x,y) x(y).', obj.external_variables, obj.adjacent_bins(2:end), 'uni', false)];
        end
    end
    
    % Split the input and output signals into both excited and adjacent frequencies
    % The first basis is the frequency axis. Yhe other basisses are selected 
    % from the external cells from the back to the front
    dimsbefore = repmat({':'}, 1, ndims(obj.input.Data)-(numel(obj.excited_bins)-1));
    index_data = [dimsbefore, {obj.excited_bins{end:-1:2}}];
    Rexc = obj.input.selectExcitedBins(obj.excited_bins{1});
    Zexc = obj.output.selectExcitedBins(obj.excited_bins{1});
    Rexc = Rexc.selectSubsetCell(index_data);
    Zexc = Zexc.selectSubsetCell(index_data);
    if ~isempty(obj.adjacent_bins)
        Radj = obj.input.selectExcitedBins(obj.adjacent_bins{1});
        Zadj = obj.output.selectExcitedBins(obj.adjacent_bins{1});
        Radj = Radj.selectSubsetCell(index_data);
        Zadj = Zadj.selectSubsetCell(index_data);
    end
    
    % Initialise the DataContainers
    aux = Rexc;
    obj.R = aux;
    obj.R = obj.R.run(@(x) zeros(Nr, 1, Ftot));
    obj.Z = aux;
    obj.Z = obj.Z.run(@(x) zeros(Nz, 1, Ftot));
    obj.CZNoise = aux;
    obj.CZNoise = obj.CZNoise.run(@(x) zeros(Nz, Nz, Ftot));
    if isempty(obj.adjacent_bins)
        obj.CZMean = aux;
        obj.CZMean = obj.CZMean.run(@(x) zeros(Nz, Nz, Ftot));
    end
    if ~isempty(obj.local_model_order)
        obj.G = aux;
        obj.G = obj.G.run(@(x) zeros(Nz, Nr, Ftot));
        obj.CvecG = aux;
        obj.CvecG = obj.CvecG.run(@(x) zeros(Nz*Nr, Nz*Nr, Ftot));
        model_coefficients_G = aux;
        model_coefficients_G = model_coefficients_G.run(@(x) zeros(Nz, parameters_model, Ftot));
    end
    if ~isempty(obj.local_transient_order)
        obj.T = aux;
        obj.T = obj.T.run(@(x) zeros(Nz, 1, Ftot));
        obj.CT = aux;
        obj.CT = obj.CT.run(@(x) zeros(Nz, Nz, Ftot));
        model_coefficients_T = aux;
        model_coefficients_T = model_coefficients_T.run(@(x) zeros(Nz, parameters_transient, Ftot));
    end
    if ~isempty(obj.output_noise_variance)
        obj.CvecGNoise = aux;
        obj.CvecGNoise = obj.CvecGNoise.run(@(x) zeros(Nz*Nr, Nz*Nr, Ftot));
    end
    
    % Construct an nD-grid starting from the basis vectors
    total_grid = ndgrid(obj.excited_bins{:});
    size_total_grid = size(total_grid);
    
    % Iterate over the grid of excited points
    ind_total_grid = cell(1, ndims(total_grid));
    lm = LocalModelEstimation();
    lm.TransientOrder = obj.local_transient_order;
    lm.PolynomialOrder = obj.local_model_order;
    scaling_grid = cell(size(total_grid));
    for kk = 1:numel(total_grid)
        
        % Retrieve indici of total grid from the linear index
        [ind_total_grid{:}] = ind2sub(size_total_grid, kk);
        if isvector(total_grid)
            ind_total_grid = ind_total_grid(1);
        end
        
        % Use these linear indici to generate the local space
        if isempty(obj.adjacent_bins)
            local_grid_exc = cellfun(@(x,y,z) LocalModelling.extractLocalSpace(x, y, z, false), obj.excited_bins, ind_total_grid, num2cell(obj.local_window_width), 'uni', false);
            local_grid = cellfun(@(x,y,z) (x(y+z) - x(z))/min(diff(x)), basis_values_excited, local_grid_exc, ind_total_grid, 'uni', false);
            scaling_grid([ind_total_grid{:}]) = {cell2mat(cellfun(@(x) min(diff(x)), basis_values_excited, 'uni', false))};
        else
            ind_adj_grid = cellfun(@(x,y) find(x < y(kk), 1, 'last'), obj.adjacent_bins, obj.excited_bins, 'uni', false);
            local_grid_adj = cellfun(@(x,y,z) LocalModelling.extractLocalSpace(x, y, z, true), obj.adjacent_bins, ind_adj_grid, num2cell(obj.local_window_width), 'uni', false);
            local_grid = cellfun(@(v,w,x,y,z) (v(w+x) - y(z))/min(diff(v)), basis_values_adjacent, local_grid_adj, ind_adj_grid, basis_values_excited, ind_total_grid, 'uni', false);
            scaling_grid{[ind_total_grid{:}]} = cell2mat(cellfun(@(v) min(diff(v)), basis_values_adjacent, 'uni', false));
        end
        
        % Do the expansion of the basis
        lm.Basis = local_grid;
        lm = lm.expandBasis;
        
        % Apply local grid on the input and output
        if isempty(obj.adjacent_bins)
            Rlocal = Rexc.selectExcitedBins(ind_total_grid{1} + local_grid_exc{1});
            Zlocal = Zexc.selectExcitedBins(ind_total_grid{1} + local_grid_exc{1});
            if numel(local_grid) > 1
                Rlocal = Rlocal.selectSubsetCell([dimsbefore, {local_grid_exc{end:-1:2} + ind_total_grid{end:-1:2}}]);
                Zlocal = Zlocal.selectSubsetCell([dimsbefore, {local_grid_exc{end:-1:2} + ind_total_grid{end:-1:2}}]);
            end
        else
            Rlocal = Radj.selectExcitedBins(ind_adj_grid{1} + local_grid_adj{1}); 
            Zlocal = Zadj.selectExcitedBins(ind_adj_grid{1} + local_grid_adj{1});
            if numel(local_grid) > 1
                Rlocal = Rlocal.selectSubsetCell([dimsbefore, {local_grid_adj{end:-1:2} + ind_adj_grid{end:-1:2}}]);
                Zlocal = Zlocal.selectSubsetCell([dimsbefore, {local_grid_adj{end:-1:2} + ind_adj_grid{end:-1:2}}]);
            end
        end
        lm.Input = Rlocal;
        lm.Output = Zlocal;
        
        % Add the center location if needed
        if isempty(obj.adjacent_bins)
            % Calculate the linear index of the center
            local_grid_size = cellfun(@(x) numel(x), local_grid, 'uni', false);
            center_indici = cellfun(@(x) find(x == 0), local_grid, 'uni', false);
            if isvector(total_grid)
                center_linear_index = center_indici{:};
            else
                center_linear_index = sub2ind([local_grid_size{:}], center_indici{:});
            end
            lm.CenterLocation = center_linear_index;
        else
            lm.CenterLocation = [];
        end
        
        % Add new value for the output noise covariance
        if ~isempty(obj.output_noise_variance)
            aux = obj.output_noise_variance.selectExcitedBins(obj.excited_bins{1}(ind_total_grid{1}));
            if numel(local_grid) > 1
                lm.OutputNoiseVariance = aux.selectSubsetCell([dimsbefore, ind_total_grid(end:-1:2)]);
            else
                lm.OutputNoiseVariance = aux;
            end
        end
        
        % Build the Jacobian matrices and perform the estimation
        lm = lm.buildJacobian;
        lm = lm.estimate;
        
        % Calculate the external cell index
        index_external = [dimsbefore, ind_total_grid(end:-1:2)];
        index_internal = [{':'}, {':'}, ind_total_grid{1}];
        
        % Append the DataContainers
        if isempty(obj.adjacent_bins)
            % Put Y from the local model estimation in a data container
            Y = cellfun(@(x) permute(x, [1,3,2]), lm.Y, 'uni', false);
            Y = cellfun(@(x) x(:,:,center_linear_index), Y, 'uni', false);
            
            % Select the center and put it in Z
            obj.Z = obj.Z.setSubsetCell(Y, index_external, index_internal);
        else
            obj.R = obj.R.setSubsetCell(Rexc.selectExcitedBins(kk).Data, index_external, index_internal);
            obj.Z = obj.Z.setSubsetCell(Zexc.selectExcitedBins(kk).Data, index_external, index_internal);
        end
        obj.CZNoise = obj.CZNoise.setSubsetCell(lm.CYNoise, index_external, index_internal);
        if isempty(obj.adjacent_bins) 
            obj.CZMean = obj.CZMean.setSubsetCell(lm.CYMean, index_external, index_internal);
        end
        if ~isempty(obj.local_model_order)
            obj.G = obj.G.setSubsetCell(lm.G, index_external, index_internal);
            obj.CvecG = obj.CvecG.setSubsetCell(lm.CvecG, index_external, index_internal);
            model_coefficients_G = model_coefficients_G.setSubsetCell(lm.ModelCoefficientsG, index_external, index_internal);
            if ~isempty(obj.output_noise_variance)
                obj.CvecGNoise = obj.CvecGNoise.setSubsetCell(lm.CvecGNoise, index_external, index_internal);
            end
        end
        if ~isempty(obj.local_transient_order)
            obj.T = obj.T.setSubsetCell(lm.T, index_external, index_internal);
            obj.CT = obj.CT.setSubsetCell(lm.CT, index_external, index_internal);
            model_coefficients_T = model_coefficients_T.setSubsetCell(lm.ModelCoefficientsT, index_external, index_internal);
        end
    end
    
    % Define the LocalModelInterpolation objects
    if ~isempty(obj.local_model_order)
        obj.ModelInterpolationG = LocalModelInterpolation();
        obj.ModelInterpolationG.ModelCoefficients = LocalModelling.implodeCellToMatrix(basis_values_excited, model_coefficients_G.Data);
        obj.ModelInterpolationG.Symbolic = lm.SymbolicG;
        obj.ModelInterpolationG.ScalingGrid = scaling_grid;
    end
    if ~isempty(obj.local_transient_order)
        obj.ModelInterpolationT = LocalModelInterpolation();
        obj.ModelInterpolationT.ModelCoefficients = LocalModelling.implodeCellToMatrix(basis_values_excited, model_coefficients_T.Data);
        obj.ModelInterpolationT.Symbolic = lm.SymbolicT;
        obj.ModelInterpolationT.ScalingGrid = scaling_grid;
    end
end