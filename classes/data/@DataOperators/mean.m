function obj = mean(obj,string)
% Take the MEAN of a DataContainer over either Periods or Realisations.

validStrings = {'Realisations','Periods'};

if isa(obj,'DataContainer')
    validString = validatestring(string,validStrings);
    switch validString
        case validStrings{1} %Realisations
            mean_dim = @(k) mean(cat(4,obj.Data{:,k}),4);
            X = size(obj.Data,2);
        case validStrings{2} %Periods
            mean_dim = @(k) mean(cat(4,obj.Data{k,:}),4);
            X = size(obj.Data,1);
        otherwise
            error('Input string was not recognized!')
    end
    obj.Data = arrayfun(mean_dim ,1:X,'uni',false);
    obj.Data = obj.Data.';
else
    error([mfilename ' functionality was not yet implemented for this class.']);
end

end