classdef LocalModelEstimation
    
    properties (Dependent) % CamelCase
        Basis               % Local parameter space
        PolynomialOrder    % Polynomial powers
        TransientOrder      % Order of the transient term
        Input               % Input
        Output              % Output
        OutputNoiseVariance % Output noise variance needed to calculate the noise variance contribution
        CenterLocation      % Indici of the location around which the series expansion is carried out
    end
    
    properties (GetAccess = public, SetAccess = protected)
        Theta
        ModelCoefficientsG
        ModelCoefficientsT
        SymbolicG
        SymbolicT
        U
        InverseEigenValues
        V
        Y
        G
        T
        CYNoise
        CYMean
        CvecG
        CT
        CvecGNoise
    end
    
    properties (Access = protected) % snake_case
        basis
        polynomial_order
        transient_order
        input
        output
        output_noise_variance
        center_location
        modelling_domain
        basis_input
        basis_transient
        jacobian
        theta
        nu
        ny
    end
    
    properties (Constant, Access = protected)
        FREQUENCY_DOMAIN_OPTIONS = {'CONTINUOUS','DISCRETE'}
    end
    
    
    %% CONSTRUCTOR
    methods
        
        function obj = LocalModelEstimation(varargin)
            p = inputParser;
            p.StructExpand = true;
            p.FunctionName = 'LocalModelEstimation';
            p.PartialMatching = true;
            p.addParameter('basis'  ,{}, @(x) iscell(x));
            p.addParameter('polynomialOrder', 2, @(x) isvector(x));
            p.addParameter('transientOrder', [], @(x) isvector(x));
            p.addParameter('input'  ,{}, @(x) isa(x,'DataContainer') || isa(x,'MultisineSignal') || iscell(x));
            p.addParameter('output' ,{}, @(x) isa(x,'DataContainer') || isa(x,'MultisineSignal') || iscell(x));
            p.addParameter('outputNoiseVariance' ,{}, @(x) isa(x,'DataContainer'));
            p.addParameter('centerLocation', [], @(x) isvector(x));
            
            p.parse(varargin{:});
            args = p.Results;
            
            obj.basis = args.basis;
            obj.polynomial_order = args.polynomialOrder;
            obj.transient_order = args.transientOrder;
            obj.Input  = args.input;
            obj.Output = args.output;
            obj.output_noise_variance = args.outputNoiseVariance;
            obj.center_location = args.centerLocation;
        end
        
    end
    
    %% OTHER METHODS
    
    methods (Static, Access = protected)
        
        function [product_polynomials, symbolic_combinations] = constructProductPolynomials(basis, orders)

            % Total number of different basis functions
            basis_order = numel(basis(:));
            
            % Iteratively build the polynomial products
            product_polynomials = 1;
            previous_length = 1;
            previous_R = 0;
            symbolic_combinations = 1;
            symbolic_variables = sym('x', [1 basis_order]);
            for i_basis = 1:basis_order

                R = orders(i_basis);
                local_basis = basis{i_basis}(:).';
                length_basis = numel(local_basis);
                
                % Retrieve the polynomial powers of the current basis
                stacked_powers = zeros(1, length_basis * (R+1));
                for j_power = 1:R+1
                    stacked_powers(1,length_basis*(j_power-1)+1:length_basis*j_power) = local_basis.^(j_power-1);
                    symbolic_basis(j_power) = symbolic_variables(i_basis)^(j_power-1); 
                end
                
                % Generate symbolic expression for later use in the
                % interpolation stage
                symbolic_combinations = symbolic_combinations(:)*symbolic_basis;

                % Multiple the current basis with the previous stack of basis
                % polynomials
                product_polynomials = product_polynomials * stacked_powers;
                
                % Convert the basis_expansion to a column vector with all the different
                % contributions grouped
                aux = zeros(prod([R+1, previous_R+1, length_basis, previous_length]), 1);
                index = 0;
                for new_basis = 1:R+1
                    for old_basis = 1:previous_R+1
                        index = index + 1;
                        block = product_polynomials((old_basis-1)*previous_length+1:old_basis*previous_length, (new_basis-1)*length_basis+1:new_basis*length_basis);
                        aux(length_basis*previous_length*(index-1)+1:length_basis*previous_length*index) = block(:);
                    end
                end
                product_polynomials = aux;

                previous_length = length_basis * previous_length;
                previous_R = R;
            end
            
            product_polynomials = reshape(product_polynomials, [previous_length, prod(orders+1)]);
            symbolic_combinations = symbolic_combinations(:);
        end
        
        function signal = implodeCellToMatrix(basis, signal)
            if numel(basis) > 1
                collapse_dimension = numel(basis) - 1;
                while collapse_dimension > 0

                    internal_size = size(signal{1});
                    external_size = size(signal);

                    % Decrement external size by one
                    aux_cell = cell(external_size(1:end-1));

                    % Augment internal size by one
                    aux_cell = cellfun(@(x) zeros([internal_size, external_size(end)]), aux_cell, 'uni', false);
                    
                    % Other dimensions of signal
                    dims_signal = repmat({':'}, 1, ndims(signal)-1);

                    % Convert cell dimension to matrix dimension
                    for i_cell = 1:external_size(end)
                        aux_cell = cellfun(@(x,y) LocalModelEstimation.implodeDimension(x,y,i_cell), signal(dims_signal{:}, i_cell), aux_cell, 'uni', false);
                    end
                    
                    % Shift the internal dimensions
                    signal = aux_cell;
                    
                    collapse_dimension = collapse_dimension - 1;
                end
            end
        end
        
        function y = implodeDimension(x,y,index)
            dims_x = repmat({':'}, 1, ndims(x));
            dims_y = repmat({':'}, 1, ndims(y)-1);
            
            y(dims_y{:}, index) = x(dims_x{:});
        end
        
        % Multiply the input with the basis functions to derive the
        % Jacobian matrix for each cell
        function jacobian = multiplyInputBasis(input, basis_input, basis_transient)
            
            Nu = size(input,1);
            
            % Pre-allocate memory for the Jacobian matrix
            if ~isempty(basis_input) && ~isempty(basis_transient)
                jacobian = zeros(Nu * size(basis_input,2) + size(basis_transient,2) , size(basis_input,1));
            elseif ~isempty(basis_input)
                jacobian = zeros(Nu * size(basis_input,2), size(basis_input,1));
            elseif ~isempty(basis_transient)
                jacobian = zeros(size(basis_transient, 2), size(basis_transient,1));
            end
            
            if ~isempty(basis_input)
                % Convert input variable from a +3D-matrix to a 2D matrix and
                % repeat different times to be able to multiply with the
                % basis_input
                stacked_input = repmat(reshape(input, Nu, []).', 1, size(basis_input,2));

                % Perform the multiplication of the basis with the input
                jacobian(1:Nu * size(basis_input,2), :) = transpose(repelem(basis_input, 1, Nu) .* stacked_input);
            end
            if ~isempty(basis_transient)
                jacobian(end-size(basis_transient,2)+1:end, :) = transpose(basis_transient);
            end
        end
        
        function [theta, U, inv_sigma, V] = computeTheta(output, jacobian)
            
            % Stack the grid points for the output
            output = reshape(output, size(output,1), []);
            
            % Normalise the rows of the Jacobian matrix with the 2-norm of
            % each row   
            scale = sum(abs(jacobian.^2), 2).^0.5;
            scale(scale == 0) = 1;
            jacobian = bsxfun(@rdivide, jacobian, scale);
            
            % Numerical stable LS estimate of the model parameters
            [U, S, V] = svd(jacobian', 0);
            sigma = diag(S);
            sigma(sigma == 0) = inf;
            inv_sigma = diag(1./sigma);
            theta = output * (U * inv_sigma * V');
            
            % Rescale the coefficients and V for further use
            theta = bsxfun(@rdivide, theta, scale.');
            V = bsxfun(@rdivide, V, scale);
        end
        
        function [Y, CYNoise, CYMean, CvecG, CT, CvecGNoise] = deriveCovariances(output, U, inv_sigma, V, CY_data, center, dof, nu, R, T)
                        
            % Stack the grid points for the output
            output = reshape(output, size(output,1), []);
            
            % Derive the output noise covariance
            Y = (output * U) * U';
            error = output - Y;
            CYNoise = (error * error') / dof;
            
            % Derive the covariance of vec(G)
            if (R > 0) && (nu > 0)
                VV = V(1:nu,:) * inv_sigma;
                CvecG = kron(conj(VV * VV'), CYNoise);
                if ~isempty(CY_data)
                    CvecGNoise = kron(conj(VV * VV'), CY_data);
                end
            else
                CvecG = [];
            end
            
            % Derive the covariance of T
            if T > 0
                CT = norm(V(end-T+1,:).' .* diag(inv_sigma), 2)^2 * CYNoise;
            else
                CT = [];
            end
            
            % Assign CvecGNoise if not needed
            if isempty(CY_data)
                CvecGNoise = [];
            end
            
            % Calculate sample covariance of the generalised sample mean
            if ~isempty(center)
                CYMean = real(U(center,:) * U(center,:)') * CYNoise;
            else
                CYMean = [];
            end
        end
    end
    
    %% GETTERS AND SETTERS
    
    methods
        
        % GET and SET Basis
        function basis = get.Basis(obj)
            basis = obj.basis;
        end
        
        function obj = set.Basis(obj, basis)
            if ~iscell(basis)
                error('Basis was not a cell.');
            else
                vectorCheck = cellfun(@(x) ~isvector(x), basis, 'uni', false);
                if any([vectorCheck{:}])
                    error('Basis should contain vectors only.');
                end
            end
            obj.basis = basis;
        end
        
        % GET and SET PolynomialOrders
        function order = get.PolynomialOrder(obj)
            order = obj.polynomial_order;
        end
        
        function obj = set.PolynomialOrder(obj, order)
            if ~isvector(order) && ~isempty(order)
                error('PolynomialOrder was not a vector.');
            end
            obj.polynomial_order = order;
        end
        
        % GET and SET TransientOrder
        function transient_order = get.TransientOrder(obj)
            transient_order = obj.transient_order;
        end
        
        function obj = set.TransientOrder(obj, transient_order)
            if ~isvector(transient_order) && ~isempty(transient_order)
                error('TransientOrder was not a scalar.');
            end
            obj.transient_order = transient_order;
        end
        
        % GET and SET Input
        function input = get.Input(obj) 
            input = obj.input;
        end
        
        function obj = set.Input(obj, input)
            input = LocalModelEstimation.modifyData(input);
            if ~LocalModelEstimation.check_DataConsistency(input)
                error('Input data was not consistent.');
            end
            obj.input = input;
            if ~isempty(input)
                [obj.nu, ~] = size(input{1});
            end
        end
        
        % GET and SET Output
        function output = get.Output(obj) 
            output = obj.output;
        end
        
        function obj = set.Output(obj, output)
            output = LocalModelEstimation.modifyData(output);
            if ~LocalModelEstimation.check_DataConsistency(output)
                error('Output data was not consistent.');
            end
            obj.output = output;
            if ~isempty(output)
                [obj.ny, ~] = size(output{1});
            end
        end
        
        % GET and SET OutputNoiseVariance
        function output_noise_variance = get.OutputNoiseVariance(obj) 
            output_noise_variance = obj.output_noise_variance;
        end
        
        function obj = set.OutputNoiseVariance(obj, output_noise_variance)
            if ~isa(output_noise_variance, 'DataContainer')
                error('OutputNoiseVariance is not a DataContainer.');
            end
            obj.output_noise_variance = output_noise_variance.Data;
        end
        
        % GET and SET CenterLocation
        function center_location = get.CenterLocation(obj)
            center_location = obj.center_location;
        end
        
        function obj = set.CenterLocation(obj, center_location)
            if ~isvector(center_location) && ~isempty(center_location)
                error('CenterLocation is not a vector.');
            end
            obj.center_location = center_location;
        end
        
    end
    
    %% CHECKERS
    
    methods (Static , Access = protected)
        
        function data = modifyData(data)
            switch class(data)
                case 'DataContainer'
                    data = data.Data;
                case 'MultisineSignal'
                    data = data.MultisineContainer.Data;
                case 'cell'
                    
                otherwise
                    error('The data does not belong to the correct class.')
            end
            
            % Treat the number of experiments as an external cell instead
            % of in the matrix
            if ~isempty(data)
                ne = size(data{1}, 2);
                aux = cell([size(data), ne]);
                otherdims = repmat({':'}, 1, ndims(data));
                for ii = 1:ne
                    aux(otherdims{:}, ii) = cellfun(@(x) permute(x(:,ii,:), [1,3,2]), data, 'uni', false);
                end
                data = aux;
                
                % Shift the dimensions to put the number of experiments after
                % the periods
                if ndims(data) > 3
                    indici = 1:ndims(data);
                    indici(3) = indici(end);
                    indici(4:end) = 3:ndims(data)-1;
                    data = permute(data, indici);
                end
            end
            

        end
        
        function res = check_DataConsistency(data)
            if ~isempty(data)
                data_size = size(data{1});
                check = cellfun(@(x) size(x) ~= data_size, data, 'uni', false);
                if any([check{:}])
                    error('The size of the data arrays is not consistent.')
                end
            end
            res = true;
        end
    end
   
end