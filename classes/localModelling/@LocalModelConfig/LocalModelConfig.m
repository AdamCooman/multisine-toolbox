classdef LocalModelConfig
    % LOCALMODELCONFIG contains all the configuration parameters 
    % which are related to the local modelling
    
    properties (Dependent) % CamelCase
        LocalModelOrder             % Order of the FRF polynomial
        LocalTransientOrder         % Order of the transient polynomial
        LocalWindowWidth              % Parameter width over which the local model is estimated
        AdjacentBins                % All the bins of the signal
        ExcitedBins                 % Excited bins
        OutputNoiseVariance         % Provide the output noise variance to split the noise and nonlinear contributions
        ExternalVariables           % Values of the external variables
    end
    
    properties (Access = protected) %snake_case
        local_model_order = 2;
        local_transient_order = 2;
        local_window_width = 5;
        transient = false;
        adjacent_bins = [];
        excited_bins = [];
        output_noise_variance = [];
        external_variables = [];
    end
    
    %% OTHER METHODS
    methods
        
    end
    
    %% GETTERS & SETTERS
    methods
        
        % GET and SET LocalModelOrder
        function local_model_order = get.LocalModelOrder(obj)
            local_model_order = obj.local_model_order;
        end
        
        function obj = set.LocalModelOrder(obj, local_model_order)
            if mod(local_model_order,1) ~= 0
                error('LocalModelOrder is not an integer.');
            end
            obj.local_model_order = local_model_order;
        end
        
        % GET and SET LocalTransientOrder
        function local_transient_order = get.LocalTransientOrder(obj)
            local_transient_order = obj.local_transient_order;
        end
        
        function obj = set.LocalTransientOrder(obj, local_transient_order)
            if mod(local_transient_order,1) ~= 0
                error('LocalTransientOrder is not an integer.');
            end
            obj.local_transient_order = local_transient_order;
        end
        
        % GET and SET LocalWindowWidth
        function local_window_width = get.LocalWindowWidth(obj)
           local_window_width = obj.local_window_width;
        end
        
        function obj = set.LocalWindowWidth(obj, local_window_width)
            if ~isvector(local_window_width)
                error('LocalWindowWidth is not a vector.');
            end
            obj.local_window_width = local_window_width;
        end
        
        % GET and SET AdjacentBins
        function adjacent_bins = get.AdjacentBins(obj)
            adjacent_bins = obj.adjacent_bins;
        end
        
        function obj = set.AdjacentBins(obj, adjacent_bins)
            if ~isvector(adjacent_bins) && ~isempty(adjacent_bins)
                error('AdjacentBins is not a vector.');
            end
            if ~iscell(adjacent_bins) && ~isempty(adjacent_bins)
                obj.adjacent_bins = {adjacent_bins};
            else
                obj.adjacent_bins = adjacent_bins;
            end
        end
        
        % GET and SET ExcitedBins
        function excited_bins = get.ExcitedBins(obj)
            excited_bins = obj.excited_bins;
        end
        
        function obj = set.ExcitedBins(obj, excited_bins)
            if ~isvector(excited_bins) && ~iscell(excited_bins)
                error('ExcitedBins is not of the correct format.');
            end
            if ~iscell(excited_bins) && ~isempty(excited_bins)
                obj.excited_bins = {excited_bins};
            else
                obj.excited_bins = excited_bins;
            end
        end
        
        % GET and SET OutputNoiseVariance
        function output_noise_variance = get.OutputNoiseVariance(obj)
            output_noise_variance = obj.output_noise_variance;
        end
        
        function obj = set.OutputNoiseVariance(obj, output_noise_variance)
            if ~isa(output_noise_variance, 'DataContainer')
                error('OutputNoiseVariance is not a DataContainer.');
            end
            obj.output_noise_variance = output_noise_variance;
        end
        
        % GET and SET ExternalVariables
        function external_variables = get.ExternalVariables(obj)
            external_variables = obj.external_variables;
        end
        
        function obj = set.ExternalVariables(obj, external_variables)
            if ~isvector(external_variables) && ~isempty(external_variables)
                error('ExternalVariables is not a vector.');
            end
            if ~iscell(external_variables) && ~isempty(external_variables)
                obj.external_variables = {external_variables};
            else
                obj.external_variables = external_variables;
            end
        end    
        
    end
    
    %% CHECKERS
    methods(Static, Access = protected)
        
    end
    
    %% STATIC METHODS
    methods(Static, Access = protected)
        
        function output = copyLocalModelConfig(input, output)
            C = metaclass(input);
            P = C.Properties;
            for k = 1:length(P)
                if ~P{k}.Dependent && ~P{k}.Constant && strcmp(P{k}.DefiningClass.Name,'LocalModelConfig')
                    output.(P{k}.Name) = input.(P{k}.Name);
                end
            end
        end
        
        function output = getLocalModelConfig(input)
            C = metaclass(input);
            P = C.Properties;
            output = LocalModelConfig();
            for k = 1:length(P)
                if ~P{k}.Dependent && ~P{k}.Constant && strcmp(P{k}.DefiningClass.Name,'LocalModelConfig')
                    output.(P{k}.Name) = input.(P{k}.Name);
                end
            end
        end
        
    end
    
end


