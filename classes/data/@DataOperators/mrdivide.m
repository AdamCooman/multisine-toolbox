function obj1 = mrdivide(obj1, obj2)

if strcmp(class(obj1),class(obj2))
    if isa(obj1,'DataContainer')
        obj1 = obj1.run(@(x,y) mrdividex(x,y), obj2);
    else
        error([mfilename ' >> functionality was not yet implemented for this class.']);
    end
else
    error([mfilename ' >> Both objects should belong to the same object class.']);
end
end

function C = mrdividex(A, B)
    Na = size(A,1);
    Nb = size(B,1);
    F = size(A,3);
    
    C = zeros(Na, Nb, F);
    
    nonzero = find(B(1,1,:));
    Nnz = length(nonzero);
    res = zeros([Na,Nb,Nnz]);
    dummyA = full(A(:,:,nonzero));
    dummyB = full(B(:,:,nonzero));
    for idx = 1:size(dummyB,3)
        res(:,:,idx) = dummyA(:,:,idx)/dummyB(:,:,idx);
    end
    C(:,:,nonzero) = res;
end
