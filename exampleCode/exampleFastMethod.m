
clear variables
close all
clc

%% GENERAL SETTINGS

fs = 10e6; % Sampling frequency
P  = 3;    % Number of periods

%% GENERATE ALL SETTINGS.

settingsInst = DataSettings();
settingsInst.FrequencySettings.fs = fs;
settingsInst.NumberOfPeriods = P;
settingsInst.NumberOfInputs = 2;
settingsInst.NumberOfOutputs = 3;
settingsInst.NumberOfRealisations = 1;
settingsInst.NumberOfExperiments = 1;

%% GENERATE ALL SIGNALS.

msConfigInst = MultisineConfig(settingsInst);
msConfigInst.GridMode = 'FULL';
msConfigInst.VoltageSettings.bin_voltage = {-40, -40};
msConfigInst.DC = {5, 5};

ms_reference = msConfigInst.generateMultisine();

%% EXPORT DATA & EMULATE SYSTEM.

exportedData_reference = ms_reference.exportData;

% Emulate a very boring system.
G = [2+1j 6; 3+6j 1; 1.25-0.1j 5];
ms_transient_input = MultisineSignal(DataContainer({[ones(1,1,400); 2*ones(1,1,400)]}, settingsInst, 'Domain', 'FREQUENCY'), settingsInst);
ms_transient_output = MultisineSignal(DataContainer({[ones(1,1,400); 2*ones(1,1,400); 3*ones(1,1,400)]}, settingsInst, 'Domain', 'FREQUENCY'), settingsInst);

% Define the reference, input and output
ms_input = 1.01.*ms_reference;
ms_output = G*ms_input;

% Expand input over the periods and add noise
ms_input = ifft(ms_input);
ms_input = ms_input.expandPeriods;
ms_input = ms_input.addNoise(1e-4);

% Expand input over the periods and add noise
ms_output = ifft(ms_output);
ms_output = ms_output.expandPeriods;
ms_output = ms_output.addNoise(1e-4);

% Convert to FREQUENCY-domain
ms_input = fft(ms_input);
ms_output = fft(ms_output);

%% LOCAL MODELLING ESTIMATION

lm = LocalModelEstimation();
lm.TransientOrder = 2;
lm.Basis = {1:400};
lm.Input = ms_input;
lm.Output = ms_output;
lm.PolynomialOrder = 2;

lm = lm.expandBasis;
lm = lm.buildJacobian;
lm = lm.estimate;

%% LOCAL MODEL CONFIG

lmConfig = LocalModelConfig();
lmConfig.ExcitedBins = 202:257;
lmConfig.LocalTransientOrder = 2;
lmConfig.LocalWindowWidth = 6;

%% LOCAL POLYNOMIAL METHOD

LM = LocalModelling(settingsInst, lmConfig,'out', ms_output, 'in', exportedData_reference);
LM = LM.run();

%% FAST METHOD USING LPM

LTI_responseInst = LTI_Response(settingsInst,'ref', exportedData_reference, 'out',ms_output,'in',ms_input);
LTI_responseInst.ExcitedBins = lmConfig.ExcitedBins;
LTI_responseInst = LTI_responseInst.useFastMethod(lmConfig);

%% PLOT RESULTS

plot(LTI_responseInst.BLA, 'Format', 'r')
hold on
plot(LTI_responseInst.BLA, 'Format', 'i')
hold off

plot(LTI_responseInst.BLA)
hold on
plot(LTI_responseInst.CovarianceBLA.Noise.run(@sqrt))
hold off

plot(LTI_responseInst.CovarianceZ.Stochastic)

plot(diag(LTI_responseInst.CovarianceY.Stochastic))
hold on
plot(LTI_responseInst.Y.Mean)
plot(diag(LTI_responseInst.CovarianceY.Noise))
hold off
