function obj1 = minus(obj1, obj2)
% MINUS substracts two objects from each other.

if strcmp(class(obj1),class(obj2))
    if isa(obj1,'DataContainer')
        if obj1.check_compatibility(obj1,obj2)
            % Apply the function to all the internal matrices at once
            obj1.Data = cellfun(@(x,y) x-y, obj1.Data ,obj2.Data, 'uni', false);
        end
    elseif isa(obj1,'MultisineSignal')
        obj1.MultisineContainer = feval(mfilename,obj1.MultisineContainer,obj2.MultisineContainer);
    else
        error([mfilename ' functionality was not yet implemented for this class.']);
    end
else
    error('Both objects should belong to the same object class.');
end

end