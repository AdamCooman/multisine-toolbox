function bins = calculateBins(obj)

%% CHECK THE INPUT ARGUMENTS.

p = inputParser;
% MultisineConfig object
p.addRequired('obj', @(x) isa(x,'MultisineConfig') );
p.parse(obj);

%% GET THE VECTOR OF NECESSARY BINS

% Get, depending on the mode, the total bins of the multisine.
bin_width = obj.grid_settings.bandwidth/obj.frequency_settings.fres;
switch obj.frequency_mode
    case 'BANDPASS'
        bins = -bin_width/2:bin_width/2;
    case 'LOWPASS'
        bins = -bin_width:bin_width;
end

%% KNOCK OUT THE UNWANTED BINS.
bins = obj.calculateGrid(obj.grid_mode, bins);

end