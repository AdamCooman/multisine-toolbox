classdef MultisineSignal < DataSettings & DataOperators
    % MULTISINESIGNAL contains all the data belonging to a multisine signal in
    % a certain domain.
    
    properties (Dependent)
        MultisineContainer % Container with Data, Domain & DomainValues
    end
    
    properties (Access = protected)
        multisine_container
    end
    
    %% CONSTRUCTOR
    methods
        
        function obj = MultisineSignal(multisineContainerInst,settingsInst)
            if  nargin ~= 0
                % Create input parser
                p = inputParser;
                p.addRequired('multisineContainerInst', @(x) isa(x,'DataContainer'));
                p.addRequired('settingsInst', @(x) isa(x,'DataSettings'));
                p.parse(multisineContainerInst,settingsInst);
                args = p.Results;
                
                obj = obj.copyDataSettings(args.settingsInst,obj);
                obj.MultisineContainer = args.multisineContainerInst;
            end
        end
        
    end
    %% OTHER METHODS
    methods
        
        function export_data = exportData(obj)
            export_data = obj.multisine_container.exportData(obj.number_of_periods);
        end 
        
        function obj = expandPeriods(obj)
            obj.multisine_container = obj.multisine_container.expandPeriods(obj.number_of_periods);
        end
        
    end
    
    %% GETTERS and SETTERS
    methods
        
        function multisine_container = get.MultisineContainer(obj)
            multisine_container = obj.multisine_container;
        end
        
        function obj = set.MultisineContainer(obj, multisine_container)
            if MultisineSignal.check_MultisineContainerConsistency(obj, multisine_container)
                obj.multisine_container = multisine_container;
            end
        end
        
    end
    
    %% CHECKERS
    methods (Static , Access = protected)
        
        function res = check_MultisineContainerConsistency(obj, multisine_container)
            N = obj.getDerivedSettings.N;
            M = obj.number_of_realisations;
            P = obj.number_of_periods;
            Ne = obj.number_of_experiments;
            
            checkOuterSize = isequal(size(multisine_container.Data),[M 1]);
            checkOuterSize = checkOuterSize || isequal(size(multisine_container.Data),[M floor(P)]);
            checkOuterSize = ~checkOuterSize;
            checkInternalSize = cellfun(@(x) ~(size(x,2) == Ne && size(x,3) == N),multisine_container.Data,'uni',false);
            if any([checkInternalSize{:}]) || checkOuterSize
                error('Size of the data_container was inconsistent with the DataSettings.')
            end
            
            res = true;
        end
        
    end
    
end


