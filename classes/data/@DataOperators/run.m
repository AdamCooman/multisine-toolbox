function obj = run(obj, fun, varargin)
% RUN any function on the internal matrices of one or multiple DataContainers.
if nargin < 2
    error('Not enough input arguments.');
end

if isa(obj,'DataContainer')
    if isempty(varargin)
        if issparse(obj.Data{1,1})
            % Convert to full matrix
            obj.Data = cellfun(@full, obj.Data, 'uni', false);
        end
        % Apply the function to all the internal matrices at once
        obj.Data = cellfun(fun, obj.Data, 'uni', false);
        
    else % varargin was not empty
        % Check if all elements in varargin are Datacontainers
        inner_types = cellfun(@(x) ~isa(x,'DataContainer'),varargin,'uni',false);
        if any([inner_types{:}])
            error([mfilename ' >> Cell elements were not DataContainers.'])
        end
        % Add the original to the varargin as well.(As the first one!)
        varargin = [{obj} varargin];
        % Check if number of inputs of anonymous function coincides with the
        % number of cell elements.
        if length(varargin) ~= nargin(fun)
            error([mfilename ' >> Cell dimension and number of function inputs were inconsistent.'])
        end
        % Convert all elements to full matrici & extract Data.
        dummy = cellfun(@(x) cellfun(@full, x.Data, 'uni', false),varargin,'uni',false);
        
        % Apply the function to the extracted dummy cell array.
        obj.Data = cellfun(fun, dummy{:}, 'uni', false);
    end
else
    error([mfilename ' >> Functionality was not yet implemented for this object class.']);
end

if strcmp(obj.Domain,'FREQUENCY') && ~obj.locked
    % Make it sparse again.
    obj.Data = cellfun(@ndSparse, obj.Data, 'uni', false);
end

end