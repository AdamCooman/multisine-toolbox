%% Test the DataContainer together with the DataSettings and DataOperators

% Data
x = 1:100;
y = {ones(3, 1, 100), 2*ones(3, 1, 100), 3*ones(3, 1, 100), 4*ones(3, 1, 100)};

% Settings
settingsInst = DataSettings();
settingsInst.FrequencySettings.fs = 1;
settingsInst.FrequencySettings.fres = 1/100;
settingsInst.NumberOfInputs = 1;
settingsInst.NumberOfOutputs = 3;
settingsInst.NumberOfExperiments = 1;
settingsInst.NumberOfRealisations = 1;
settingsInst.NumberOfPeriods = 2;

% Get derived settings
derivedSettings = settingsInst.getDerivedSettings();

% Initialise DataContainer
z = DataContainer(y, settingsInst);

% Plot z in TIME-domain
plot(z, 'Subset', {1,2:3}, 'Format', {'r', 'i'}, 'XLabel', 'Time [s]')

% Apply operators on z
w = z + z;
w = z - z;
w = 2 .* z;
w = zeros(1,3,100) * z;
w = z.run(@(x) (x/2).^2);

% Convert DataContainer from TIME-domain to FREQUENCY-domain
z.Domain = 'FREQUENCY';
plot(z, 'XLabel', 'Frequency [Hz]')

