classdef DataContainer < DataOperators
    % DATACONTAINER contains all the data belonging to a signal in
    % a certain domain.
    
    properties (Dependent)
        Data                    % contains the data in a cell format.
        Domain                  % TIME or FREQUENCY
    end
    
    properties (Access = protected)
        data
        domain = 'FREQUENCY'
    end
    
    properties (GetAccess = public, SetAccess = protected)
        DomainValues
    end
    
    properties (Hidden = true)
        fs
        fres
        locked = false          % Locked multisines have fixed properties and can't be sparse.
        expanded = false        % Was the data periodically expanded?
    end
    
    properties (Constant, Access = protected)
        DOMAIN_OPTIONS = {'TIME','FREQUENCY'}
    end
    
    %% CONSTRUCTOR
    methods
        
        function obj = DataContainer(data, settingsInst, varargin)
            if  nargin ~= 0
                % Create input parser
                p = inputParser;
                p.PartialMatching = true;
                p.StructExpand = true;
                p.addRequired('data', @DataContainer.check_DataConsistency);
                p.addRequired('settingsInst', @(x) isa(x,'DataSettings'));
                p.addParameter('domain', 'TIME', @(x) any(strcmpi(x,DataContainer.DOMAIN_OPTIONS)));
                p.addParameter('split_periods', false, @(x) islogical(x));
                p.parse(data, settingsInst, varargin{:});
                args = p.Results;
               
                obj.data = args.data;
                if strcmp(args.domain, 'FREQUENCY') && args.split_periods
                    error('When splitting periods, the domain should be TIME.')
                end
                obj.domain = args.domain;
                obj.fs = settingsInst.FrequencySettings.fs;
                obj.fres = settingsInst.FrequencySettings.fres;
                
                if args.split_periods
                    N = args.settingsInst.getDerivedSettings.N;
                    P = args.settingsInst.NumberOfPeriods;
                    culling_length = floor(P)*N;
                    
                    % Seperate the periods in different
                    % Throw away the fractional part of the period.
                    cull_data = @(x) x(:,:,end-culling_length+1:end);
                    obj = obj.run(cull_data);
                    obj = obj.splitPeriods(P);
                    
                    obj.expanded = true;
                end
                
                obj = obj.updateDomainValues;
            end
        end
        
    end

   %% CHECKERS
    methods (Static , Access = protected)
        
        function res = check_DataConsistency(data)
            % Check the consistency of the lengths along all fields.
            if ~isempty(data)
                % Check whether all the data cells have the same matrix
                % sizes.
                data_size = size(data{1});
                check = cellfun(@(x) size(x) ~= data_size, data, 'uni', false);
                if any([check{:}])
                    error('The size of the data arrays is not consistent.')
                end
                if ~ismatrix(data{1}) && ndims(data{1}) ~= 3
                    error('The data array should be a 2D or 3D matrix.')
                end
            end
            res = true;
        end
        
        function x = setExcitedbins(x, y, index_internal)
            x(index_internal{:}) = y;
        end
           
        function x = setPeriodData(x, y, index)
            x(:,:,index) = y;
        end
    end
    
    %% OTHER METHODS
    
    methods
        
        function data = exportData(obj,number_of_periods)
            if strcmp(obj.domain,'FREQUENCY')
                %Convert to the time domain.
                obj = ifft(obj);
            end
            P = number_of_periods;
            N = obj.fs/obj.fres;
            % We have to expand it across the periods.
            period_ceil = ceil(P);
            
            %Repeat the data in function of the periods.
            repmat_dim = @(x) repmat(x,[1,1,period_ceil]);
            obj = obj.run(repmat_dim);
            %Remove a part to end up with a fractional number of periods.
            subarray = @(x) x(:,:,end-P*N+1:end);
            obj = obj.run(subarray);
            obj = obj.run(@full);
            
            data = obj.data;
        end
        
        function obj = importData(~, data, settingsInst)
            obj = DataContainer(data, settingsInst, 'domain', 'TIME');
        end
        
        function obj = expandPeriods(obj,number_of_periods)
            if obj.expanded
                warning('DataContainer was already expanded, ignoring call.')
            else
                obj.expanded = true;
                P = number_of_periods;
                
                %Repeat the data in function of the periods.
                obj.Data = repmat(obj.Data,[1,floor(P)]);
            end
        end
        
        function obj = splitPeriods(obj,number_of_periods)
            P = number_of_periods;
            N = obj.fs/obj.fres;
            Nu = size(obj.Data{1},2);
            
            % Remove the fractional part of the periods
            obj.data = cellfun(@(x) x(:,:,end-floor(P)*N+1:end), obj.data, 'uni', false); 
            size_data = size(obj.data);
            
            % Initialize expanded object
            dims_after_periods = repmat({':'}, 1, ndims(obj.data)-2);
            if ismatrix(obj.data)
                expanded_data = cell(size_data(1), floor(P));
            else
                expanded_data = cell(size_data(1), floor(P), size_data(3:end)); 
            end
            
            % Split the different periods
            dummy = cellfun(@(x) mat2cell(x,numel(x)/Nu/N/floor(P),Nu,repmat(N,[1,floor(P)])),obj.data,'uni',false);
            dummy = cellfun(@(x) squeeze(x).' ,dummy, 'uni', false);
            for pp = 1:floor(P)
                expanded_data(:,pp,dims_after_periods{:}) = cellfun(@(x) x{1,pp}, dummy(:,1,dims_after_periods{:}),'uni', false);
            end
            obj.data = expanded_data;
            
            obj.expanded = true;
        end
        
        function obj = concatenatePeriods(obj)
            P = size(obj.Data, 2);
            N = size(obj.Data{1}, 3);
            
            % Calculate cell and data size
            size_cell = size(obj.Data);
            size_cell(2) = 1;
            size_data = size(obj.Data{1});
            size_data(3) = P * size_data(3);
            
            dummy = obj.Data;
            otherdims = repmat({':'}, 1, ndims(dummy)-2);
            obj.data = cell(size_cell);
            obj = obj.run(@(x) zeros(size_data));
            for pp = 1:P
                if isempty(otherdims)
                    obj.data{:, 1}(:,:,(pp-1)*N+1:pp*N) = dummy{:,pp};
                else
                    obj.data = cellfun(@(x,y) DataContainer.setPeriodData(x,y,(pp-1)*N+1:pp*N),obj.data(:,1,otherdims{:}), dummy(:,pp,otherdims{:}), 'uni', false);
                end
            end
            obj.fres = obj.fres / P;
            obj = obj.updateDomainValues;
        end
        
        
        
        function obj = selectExcitedBins(obj,excited_bins)
            select_func = @(x) x(:,:,excited_bins);
            obj = obj.run(select_func);
            obj.DomainValues = obj.DomainValues(excited_bins);
            
            % From now on the DataContainer is locked!
            % - Can't be sparse anymore.
            % - DomainValues do not auto-update.
            % - Can't change domain to TIME any longer.
            obj.locked = true;
            obj = obj.run(@full);
        end
        
        function data_size = getDataSize(obj)
            data_size = size(obj.data{1});
        end
        
        function obj = selectSubsetCell(obj, index)
            obj.data = obj.data(index{:});
        end
        
        function obj = setSubsetCell(obj, data, index_external, index_internal)
            aux = obj.selectSubsetCell(index_external);
            obj.data(index_external{:}) = cellfun(@(x,y) DataContainer.setExcitedbins(x, y, index_internal), aux.data, data, 'uni', false);
        end
    end
    
    
    %% GETTERS and SETTERS
    methods
        function data = get.Data(obj)
            data = obj.data;
        end
        
        function obj = set.Data(obj, data)
            if DataContainer.check_DataConsistency(data)
                obj.data = data;
                obj = obj.updateDomainValues;
            end
        end
        
        function domain = get.Domain(obj)
            domain = obj.domain;
        end
        
        function obj = set.Domain(obj, domain)
            if any(strcmpi(domain,DataContainer.DOMAIN_OPTIONS)) && ~obj.locked
                if ~strcmp(obj.domain, domain)
                    obj.domain = domain;
                    N = length(obj);
                    switch domain
                        case 'TIME'
                            % Move the DC to the beginning.
                            ifftshift_dim = @(x) ifftshift(x,3);
                            obj = obj.run(ifftshift_dim);
                            % Do the ifft.
                            ifft_dim = @(x) ifft(x,[],3);
                            obj = obj.run(ifft_dim).*N;
                        case 'FREQUENCY'
                            % Do the fft.
                            fft_dim = @(x) fft(x,[],3);
                            obj = obj.run(fft_dim)./N;
                            % Move the DC to the middle.
                            fftshift_dim = @(x) fftshift(x,3);
                            obj = obj.run(fftshift_dim);
                    end
                    obj = updateDomainValues(obj);
                end
            end
        end
        
        function obj = updateDomainValues(obj)
            if ~obj.locked
                switch obj.domain
                    case 'TIME'
                        ts = 1/obj.fs;
                        tres = 1/obj.fres;
                        obj.DomainValues = 0:ts:(tres-ts);
                    case 'FREQUENCY'
                        obj.DomainValues = -obj.fs/2:obj.fres:(obj.fs/2-obj.fres);
                end
            end
        end
    end
    
    
    %% STATIC PUBLIC METHODS
    methods (Static , Access = public)
        
        function res = check_compatibility(obj1,obj2)
            if ~strcmp(obj1.Domain,obj2.Domain)
                error('Domains of the objects were different!')
            end
            if size(obj1.Data) ~= size(obj2.Data)
                error('External Data dimensions were different.')
            end
            checkInternalSize = cellfun(@(x,y) size(x) ~= size(y),obj1.Data,obj2.Data,'uni',false);
            if any([checkInternalSize{:}])
                error('Internal Data dimensions were not compatible.')
            end
            res = true;
        end
        
    end
    
end


