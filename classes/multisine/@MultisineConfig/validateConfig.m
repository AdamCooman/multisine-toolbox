function res = validateConfig(obj)

%% CHECK THE INPUT ARGUMENTS.

p = inputParser();
p.addRequired('obj',@(x) isa(x,'MultisineConfig'))
p.parse(obj);

%% VALIDATE FREQUENCY SETTINGS

% Check the number of points.
N = obj.frequency_settings.fs / obj.frequency_settings.fres;
if rem(N,1)~=0
    error('N was not an integer number! Please verify that the division fs/fres is integer...')
end

% Check if the bandwidth is correctly chosen.
bin_width = obj.grid_settings.bandwidth / obj.frequency_settings.fres;
if rem(bin_width,1)~=0
    error('The bandwidth could not be realized using this particular frequency resolution.')
end

% Get the bins with spectral energy.
bins = calculateBins(obj);
maximum_bin = max(abs(bins));

% Check the Nyquist criterion
if maximum_bin+1 > N/2
    error('Too many excited frequencies. The Nyquist criterion is violated.');
end

%% VALIDATE SOURCE SETTING

switch obj.amplitude_mode
    case obj.AMPLITUDE_MODE_OPTIONS{1} %POWER
        power_settings = obj.power_settings;
        if numel(power_settings.peak) ~= obj.number_of_inputs...
                || numel(power_settings.average) ~= obj.number_of_inputs
            error('Every input should have an amplitude defined in the power_settings struct.')
        end
    case obj.AMPLITUDE_MODE_OPTIONS{2} %VOLTAGE
        voltage_settings = obj.voltage_settings;
        if numel(voltage_settings.bin_voltage) ~= obj.number_of_inputs
            error('Every input should have an amplitude defined in the voltage_settings struct.')
        end
end

%% RETURN TRUE
res = true;

end
