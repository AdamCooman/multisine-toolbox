function modifiedBins = calculateGrid(obj, str, bins)
% GENERATEGRID creates the multisine grid starting from the entire vector
% of supplied bins by using the constraints given in str.

%% CHECK THE INPUT ARGUMENTS.

p = inputParser;
% MultisineConfig object
p.addRequired('obj', @(x) isa(x,'MultisineConfig') );
% Properties that should be enforced on bins.
p.addRequired('str', @iscellstr );
% Array of bins in succeeding order.
p.addRequired('bins', @isvector  );
p.parse(obj, str, bins);

%% APPLY THE CONSTRAINTS ON THE BINS VECTOR.

% Use the magic of tables.
binsTable = table(bins.',(1:numel(bins)).','VariableNames',{'bins' 'indices'});

% Go over all the constraint contained in str, but in reverse!
for ii = numel(str):-1:1
    % Initialisation
    switch str{ii}
        case 'ODD'
            binsTable = binsTable(logical(rem(binsTable.bins,2)),:);
            binsTable.bins = (binsTable.bins+1)/2;
        case 'EVEN'
            binsTable = binsTable(logical(~rem(binsTable.bins,2)),:);
            binsTable.bins = binsTable.bins/2;
        case 'RANDOM'
            group_size = obj.frequency_settings.group_size;
            random_select = zeros(group_size,ceil(numel(binsTable.bins)/group_size));
            % Randomly enable one of the samples of each column.
            random_select(sparse(randi(size(random_select,1),size(random_select,2),1), 1:size(random_select,2), true))=1;
            random_select = reshape(random_select,numel(random_select),1);
            
            binsTable = binsTable(logical(random_select(1:numel(binsTable.bins))),:);
        otherwise
            % It's a FULL multisine, thus do nothing.
    end
end

modifiedBins = bins(binsTable.indices);

end