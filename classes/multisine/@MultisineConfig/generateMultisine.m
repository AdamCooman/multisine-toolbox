function multisine =  generateMultisine(obj)

%% CHECK THE INPUT ARGUMENTS.

p = inputParser;
p.addRequired('obj', @(x) isa(x,'MultisineConfig') );
p.parse(obj);

%% VALIDATE INTERNAL DEPENDENCIES.
obj.validateConfig();

%% GENERATE THE MULTISINE SIGNAL
% Start with generating the grid of the multisine.

N = obj.frequency_settings.fs / obj.frequency_settings.fres;

multisine_temp = cell(obj.number_of_realisations,1);
% multisine_temp = cell(obj.number_of_inputs,obj.number_of_inputs);

grid_struct.bins = obj.calculateBins();
% Do the bin downsampling!
grid_struct.bins = grid_struct.bins(1:obj.grid_settings.grid_downsampling:end);

grid_struct.Nb = numel(grid_struct.bins);
fprintf('Number of bins was found to be equal to %i.\n',grid_struct.Nb)

%% AMPLITUDE CONSTRAINTS - modify the multisine amplitude.

switch obj.amplitude_mode
    case obj.AMPLITUDE_MODE_OPTIONS{1} %POWER
        power_settings = obj.power_settings;
        % Calculate the necessary amplitude for each bin.
        average_wattage = cellfun(@(x) 1e-3*10^(x/10),power_settings.average,'uni',false);
        rms_voltage = cellfun(@(x) sqrt(x*50),average_wattage,'uni',false);
        grid_struct.bin_voltage = cellfun(@(x) x*sqrt(2/grid_struct.Nb),rms_voltage,'uni',false);
        
        for i_realisation = 1:obj.number_of_realisations
            realisation_temp = generateRealisation(obj,grid_struct);
            
            invalid_realisation = true;
            while invalid_realisation
                % We are optimistic and already set the invalid realisation
                % to false...
                invalid_realisation = false;
                % Checking each input and experiment for the constraints.
                for i_input = 1:obj.number_of_inputs
                    for i_experiment = 1:obj.number_of_experiments
                        ms = ifft(ifftshift(full(realisation_temp(i_input,i_experiment,:)),3))*N;
                        papr_dbm = 20*log10(max(abs(ms))/rms(ms));
                        
                        fprintf('The PAPR is: %f dB, bringing the peak power up to %f dBm \n',...
                            papr_dbm,papr_dbm+power_settings.average{i_input})
                        % Check if this particular multisine obeys the
                        % constraints.
                        if papr_dbm+power_settings.average{i_input} > power_settings.peak{i_input}
                            % The realisation sucks unfortunately...
                            disp('--> Invalid realisation...');
                            invalid_realisation = true;
                            break
                        end
                    end
                    if invalid_realisation
                        % The realisation didn't obey the constraints...
                        % Generate a new realisation, maybe this one will obey the peak
                        % constraints?
                        realisation_temp = generateRealisation(obj,grid_struct);
                        break
                    end
                end
            end
            % We succeeded!
            disp('--> SUCCESSFUL realisation...');
            multisine_temp{i_realisation,1} = realisation_temp;
        end
    case obj.AMPLITUDE_MODE_OPTIONS{2} %BIN
        voltage_settings = obj.voltage_settings;
        grid_struct.bin_voltage = cellfun(@(x) 10^(x/20),voltage_settings.bin_voltage,'uni',false);
        for i_realisation = 1:obj.number_of_realisations
            multisine_temp{i_realisation,1} = generateRealisation(obj,grid_struct);
        end
end

%% GENERATE THE ACTUAL MULTISINE INSTANCE.

data = multisine_temp;
settingsInst = obj.getDataSettings(obj);

data_container = DataContainer(data, settingsInst, 'domain', 'FREQUENCY');
multisine = MultisineSignal(data_container,settingsInst);

end

function realisation_temp = generateRealisation(obj,grid_struct)

Nb = grid_struct.Nb;
bins = grid_struct.bins;
bin_voltage = grid_struct.bin_voltage;

N = obj.frequency_settings.fs / obj.frequency_settings.fres;
if rem(N,2)
    middle_bin = (N-1)/2+1;
    start_bin = 1;
else
    middle_bin = N/2+1;
    start_bin = 2;
end

realisation_temp = ndSparse(zeros(obj.number_of_inputs,obj.number_of_experiments,N));

% Get a FOMS phase realisation.
phase_input = 2*pi*rand(1,obj.number_of_inputs, Nb); % phase for each input
phase_experiment = 2*pi*rand(1,obj.number_of_experiments,Nb); % phase for each experiment
for i_experiment = 1:obj.number_of_experiments
    for i_input = 1:obj.number_of_inputs
        bins_amplitude = bin_voltage{i_input} .* ones(numel(bins), 1);
        realisation_temp(i_input,i_experiment,bins+middle_bin) = bins_amplitude;
        
        realisation_phase =...
            phase_experiment(1,i_experiment,:) + phase_input(1,i_input,:);
        realisation_phase = ...
            realisation_phase + 2*pi*(i_experiment-1)*(i_input-1)/obj.number_of_inputs;
        
        realisation_temp(i_input,i_experiment,bins+middle_bin) = ...
            realisation_temp(i_input,i_experiment,bins+middle_bin)...
            .*exp(1j*realisation_phase);
    end
end

% Mirror the data vector around N/2 (or (N+1)/2).
switch obj.frequency_mode
    case 'LOWPASS'
        for i_experiment = 1:obj.number_of_experiments
            for i_input=1:obj.number_of_inputs
                
                % Add DC
                realisation_temp(i_input,i_experiment,middle_bin)= obj.dc;
                
                % Make the lowpass multisine real by substituting the negative
                % spectral content with the conjugate positive ones.
                realisation_temp(i_input,i_experiment,start_bin:middle_bin-1)=...
                    conj(flip(realisation_temp(i_input,i_experiment,middle_bin+1:end)));
                
            end
        end
    otherwise
        % BANDPASS is already ok.
end
end