
clear
close all
clc

%% GENERAL SETTINGS

msConfigInst = MultisineConfig();
msConfigInst.NumberOfInputs = 1;
msConfigInst.GridMode = 'FULL';
msConfigInst.VoltageSettings.bin_voltage = -40;
msConfigInst.DC = 5;
msConfigInst.NumberOfPeriods = 3.2;

%% GENERATE THE MULTISINE.

MS = msConfigInst.generateMultisine();

%% PLOT SOME VALIDATION FIGURES.

plot(ifft(MS), 'Format', 'r')

plot(ifft(MS), 'Format', {'r'})
hold on
plot(ifft(MS), 'Format', 'i')


%% EXPORT THE DATA

exportedData = MS.exportData;

