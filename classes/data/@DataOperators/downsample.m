function obj = downsample(obj, scalar)
% Downsample a DataContainer.

if isa(obj,'DataContainer')
    if strcmp(obj.Domain,'TIME')
        if isscalar(scalar) && isnumeric(scalar)
            % Check the size of the matrix
            obj.fs = obj.fs/scalar;
            obj = obj.run(@(x) downsample(x,scalar)); 
        else
            error('Type of scalar was invalid.')
        end
    else
        warning('DataContainer was in the frequency domain, ignoring downsample call.');
    end
else
    error([mfilename ' functionality was not yet implemented for this class.']);
end

end