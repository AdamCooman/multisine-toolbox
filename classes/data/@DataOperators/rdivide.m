function obj1 = rdivide(obj1, obj2)
% RDIVIDE defines the division of an object with a scalar or another object.

if strcmp(class(obj1),class(obj2))
    if isa(obj1, 'DataContainer')
        if obj1.check_compatibility(obj1,obj2)
            obj1.Data = cellfun(@(x,y) x./y, obj1.Data, obj2.Data, 'uni', false);
        end
    elseif isa(obj1, 'MultisineSignal')
        obj1.MultisineContainer = feval(mfilename,obj1.MultisineContainer,obj2.MultisineContainer);
    else
        error([mfilename ' functionality was not yet implemented for this class.']);
    end
elseif isnumeric(obj2) && isscalar(obj2)
    if isa(obj1, 'DataContainer')
        obj1.Data = cellfun(@(x) x./obj2, obj1.Data, 'uni', false);
    elseif isa(obj1, 'MultisineSignal')
        obj1.MultisineContainer = feval(mfilename,obj1.MultisineContainer,scalar);
    else
        error([mfilename ' functionality was not yet implemented for this class.']);
    end
else
    error('Both objects should either belong to the same class or the second argument should be a scalar.');
end

end