function plot(obj, varargin)
% PLOT the signal Data.
% 
% The variable Format specifies in which format the signal Data is plotted.
% It can take the following formats:
%   - m: modulus in linear scale
%   - d: modulus in decibel
%   - s: squared modulus in linear scale
%   - R: phase in radians
%   - D: phase in degrees
%   - r: real part
%   - i: imaginary part

%% Parse input parameters

% Format specifications
FORMAT_LIST = {'m','d','s','R','D','r','i'};
validateFormat = @(x) all(ismember(cellstr(x(:)), FORMAT_LIST));

% Input parser
p = inputParser;
p.addRequired('obj', @(x) isa(x,'DataContainer') || isa(x,'MultisineSignal'));
p.addParameter('Format', [], validateFormat);
p.addParameter('Xlabel', [], @ischar);
p.addParameter('Subset', {1}, @(x) iscell(x));
p.KeepUnmatched = 1;
p.parse(obj,varargin{:});
args = p.Results;

if isa(obj,'MultisineSignal')
    obj = obj.MultisineContainer;
end

% Check whether the Subset is valid
size_data = size(obj.Data);
total_subset = 1;
for ii = 1:length(args.Subset)
    if max(args.Subset{ii}) > size_data(ii)
        error('Subset exceeds the cell dimensions.')
    else
        total_subset = total_subset * numel(args.Subset{ii});
    end
end
    
% Extract the subset
obj.Data = obj.Data(args.Subset{:});
            
% Give a default format to the different domains
if isempty(args.Format)
    switch obj.Domain
        case 'TIME'
            args.Format = {'r'};
        otherwise
            args.Format = {'d'};
    end
elseif ischar(args.Format)
    args.Format = {args.Format};
end

% Add Marker and LineStyle if not specified
unmatched = p.Unmatched;
switch obj.Domain
    case 'FREQUENCY'
        if ~isfield(unmatched, 'Marker')
            unmatched.Marker = 'x';
            unmatched.LineStyle = 'none';
        end
        
    otherwise
end

%% X-axis formatting

Xlabel = args.Xlabel;
if ~isempty(obj.DomainValues)
    Xaxis = obj.DomainValues;
else
    % Get matrix sizes
    [~, matrix_sizes] = size(obj);
    Xaxis = 1:matrix_sizes(1);
end

%% Generate handle with plots in the correct format

% Sizes of the Format and the internal matrices
[q, r] = size(args.Format);
nz = size(obj.Data{1,1}, 1);
ne = size(obj.Data{1,1}, 2);

% Generate fig_subplot which contains information about which format each
% subplot within each figure has
if (q == 1) && (r == 1)
    fig_subplot = repmat(args.Format(:), [1, nz*ne]);
else
    fig_subplot = repmat(args.Format(:).', [nz*ne, 1]);
end

%% Plot the DataContainer

[number_of_figures, number_of_subplots] = size(fig_subplot);

% Put all the different ports and experiments on top of eachother
obj = obj.run(@(x) reshape(permute(x, [2,1,3]), size(x,1)*size(x,2),size(x,3)));

% Check if there exist already figures
figures = findobj('Type', 'figure');
if isempty(figures)
    figure_handle = 0;
else 
    figure_handle = numel(figures);
end

% Store the value of hold on
hold_on = ishold;

% Hold on generates a figure when no figures are already created, close the
% figure in that case
if ~figure_handle
    close
end

for i_figure = 1:number_of_figures
    if ~hold_on
        figure(max(figure_handle) + i_figure)
    end
    
    % Main loop over the different subplots
    for i_subplot = 1:number_of_subplots
        if number_of_figures > 1
            subplot(q,r,i_subplot)
        else
            subplot(nz,ne,i_subplot)
        end
        
        % Put on hold on for the different subplots
        if hold_on
            hold on
        end
        
        % Perform the correct operation on the data
        [obj_operand, Ylabel] = transformData(obj, fig_subplot{i_figure,i_subplot});
        
        % Select the internal dimension that is needed for plotting
        if number_of_figures > 1
            obj_operand = obj_operand.run(@(x) x(i_figure,:,:));
        else
            obj_operand = obj_operand.run(@(x) x(i_subplot,:,:));
        end
        
        % Convert cell to matrix
        plot_data = cell2mat(cellfun(@full, obj_operand.Data(:), 'uni', false));
        
        % Actually plot 
        plot(Xaxis(:), plot_data, unmatched)
        xlabel(Xlabel)
        ylabel(Ylabel)        
    end
end
end

function [obj_operand, Ylabel] = transformData(obj, format)

    switch format
        % Modulus in linear scale
        case 'm'
            obj_operand = obj.run(@abs);
            Ylabel = 'Amplitude (linear)';

        case 'd'
            obj_operand = obj.run(@db);
            Ylabel = 'Magnitude (dB)';

        case 's'
            obj_operand = obj.run(@(x) abs(x).^2);
            Ylabel = 'Squared magnitude (linear)';

        case 'R'
            obj_operand = obj.run(@(x) unwrap(angle(x)));
            Ylabel = 'Phase (rad)';

        case 'D'
            obj_operand = obj.run(@(x) unwrap(angle(x))/pi*180);
            Ylabel = 'Phase (deg)';

        case 'r'
            obj_operand = obj.run(@real);
            Ylabel = 'Real part';

        case 'i'
            obj_operand = obj.run(@imag);
            Ylabel = 'Imaginary part';
        otherwise
    end
end
