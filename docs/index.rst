.. Multisine Toolbox documentation master file, created by
   sphinx-quickstart on Fri Jan 25 13:25:01 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Multisine Toolbox's documentation!
=============================================

This Matlab toolbox aims to serve the following purposes:

*  Allow generation and storage of excitation data in a user-friendly format.
*  Offer a wide array of customizable multisine formats suited for almost all identifaction purposes.
*  Empower easy use of advanced identification techniques including, but not limited to:

	*  The Best Linear Approximation (BLA)
	*  The Local Rational Model (LRM) Techniques
	*  The Best Linear Parameter-Varying (LPV) modelling techniques

All techniques are available in both SISO as MIMO formats and allow for static dependency extension to other more arbitrary variables, such as input power.

Examples of usage can be found in *\exampleCode*.

Supplied data storage containers make heavy use of the nDSparse format found 
`here <https://nl.mathworks.com/matlabcentral/fileexchange/29832-n-dimensional-sparse-arrays>`_.

Tested Matlab Versions:

* 2014a
* 2018b

Contents
==================

.. toctree::
   :maxdepth: 2

   class_diagram