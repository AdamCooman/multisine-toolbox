function obj = useRobustMethod(obj)
% Calculate the BLA when the reference is known and taking into account
% different periods and realisations...
% Based on the 'Bijbel' from Rik Pintelon and Johan Schoukens

%% Find, if necessary, the excited bins

if ~isempty(obj.excited_bins)
    % Check that excited bins is within the range of the data
    if numel(obj.excited_bins) > length(obj.reference)
        error('Excited_bins is out of bound.');
    end
else
    obj = calculateExcitedBins(obj);
end

Nu = obj.NumberOfInputs;
Ny = obj.NumberOfOutputs;
P = obj.NumberOfPeriods;
M = obj.NumberOfRealisations;

%% Calculate the BLA

% Only select the excited bins.
R = obj.reference.selectExcitedBins(obj.excited_bins);
U = obj.input.selectExcitedBins(obj.excited_bins);
Y = obj.output.selectExcitedBins(obj.excited_bins);

% Create the stacked Z vector.
Z = LTI_Response.stackZ(Y,U);

% Turn input/output spectra with the phase of the reference signal
Z = Z / R;

% Calculate the mean over the different periods
obj.Z.Periods = mean(Z, 'Periods');

% Average over the different realisations
obj.Z.Mean = mean(obj.Z.Periods, 'Realisations');

% Calculate the BLA
[obj.Y.Mean, obj.U.Mean] = LTI_Response.splitZ(obj.Z.Mean, Ny);
obj.BLA = obj.Y.Mean / obj.U.Mean;

%% Covariance calculation

% Input-output residuals over periods
dummy = obj.Z.Mean;
dummy.Data = repmat(dummy.Data, [M, floor(P)]);
residuals_periods = Z - dummy;

% Input-output residuals mean
dummy = obj.Z.Mean;
dummy.Data = repmat(dummy.Data, [M, 1]);
residuals_mean = obj.Z.Periods - dummy;

% Calculate the noise covariance of the Z matrix
if P > 1
    obj.CovarianceZ.Noise = cov(residuals_periods, 'Periods');
    obj.CovarianceZ.Noise = mean(obj.CovarianceZ.Noise, 'Realisations');
end

% Calculate the total covariance of the Z matrix
obj.CovarianceZ.Total = cov(residuals_mean, 'Realisations');

% Calculate the total and noise variance of the BLA
CvecBLA = obj.BLA.run(@(x, y, z) obj.calculateCovarianceMatrix(x, y, z), obj.Z.Mean, obj.CovarianceZ.Total);
obj.CovarianceBLA.Total = CvecBLA.run(@(x) obj.selectDiagonalCovarianceMatrix(x, Ny, Nu, true));
if P > 1
    CvecBLANoise = obj.BLA.run(@(x, y, z) obj.calculateCovarianceMatrix(x, y, z), obj.Z.Mean, obj.CovarianceZ.Noise);
    obj.CovarianceBLA.Noise = CvecBLANoise.run(@(x) obj.selectDiagonalCovarianceMatrix(x, Ny, Nu, true));
    obj.CovarianceBLA.Stochastic = LTI_Response.deriveStochasticDistortions(obj.CovarianceBLA.Total, obj.CovarianceBLA.Noise);
end

% Calculate U, Y and covariances
[obj.CovarianceY.Total, obj.CovarianceU.Total] = LTI_Response.splitCovarianceZ(obj.CovarianceZ.Total, Ny, Nu);
if P > 1
    [obj.CovarianceY.Noise, obj.CovarianceU.Noise] = LTI_Response.splitCovarianceZ(obj.CovarianceZ.Noise, Ny, Nu);
    obj.CovarianceZ.Stochastic = LTI_Response.deriveStochasticDistortions(obj.CovarianceZ.Total, obj.CovarianceZ.Noise);
end

end