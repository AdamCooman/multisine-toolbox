function obj = buildJacobian(obj)
% Build the Jacobian matrix needed for the estimation procedure
% 1D case: retrieve a Jacobian for all the different cells seperately

% Check if basis expansion was carried out
if isempty(obj.basis_input) && isempty(obj.basis_transient)
    error('The basis expansion was not carried out.');
end

% Depending on the number of vectors in Basis, convert part of the input
% cell to the internal matrix
obj.input = LocalModelEstimation.implodeCellToMatrix(obj.basis, obj.input);

% Retrieve the Jacobian matrix for each cell separately 
obj.jacobian = cellfun(@(x) LocalModelEstimation.multiplyInputBasis(x, obj.basis_input, obj.basis_transient), obj.input, 'uni', false); 

end

