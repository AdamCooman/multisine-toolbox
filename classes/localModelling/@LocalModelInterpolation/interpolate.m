function obj = interpolate(obj)
    
    % Check input variables
    if isempty(obj.interpolation_values)
        error('No InterpolationValues specified.');
    end
    
    % Find the nearest point to the grid
    nearest_points = cell(numel(obj.interpolation_values), numel(obj.grid_values));
    for ii = 1:numel(obj.grid_values)
        for jj = 1:numel(obj.interpolation_values)
            nearest_points(jj, ii) = cellfun(@(x) findClosest(x - obj.interpolation_values{jj}(ii)), obj.grid_values(ii), 'uni', false);
        end
    end
    nearest_points = mat2cell(cell2mat(nearest_points), ones(1, numel(obj.interpolation_values)), numel(obj.grid_values));
    
    % Loop over the different InterpolationValues
    for ii = 1:numel(nearest_points)
       
        % Get the corresponding ScalingGrid point
        scaling_grid = obj.scaling_grid{nearest_points{ii}};
        
        % Local frequency variable
        local_frequency = zeros(1, numel(obj.grid_values));
        for jj = 1:numel(obj.grid_values)
            local_frequency(jj) = (obj.interpolation_values{ii}(jj) - obj.grid_values{jj}(nearest_points{ii}(jj)))./scaling_grid(jj);
        end
        
        % Evaluate the symbolic expressions
        symbolic_variables = sym('x', [1 numel(obj.grid_values)], 'real');
        basis_expansion = double(subs(obj.symbolic, symbolic_variables, local_frequency));
        
        % Perform the actual interpolation
        switch numel(obj.grid_values)
            case 1
                obj.InterpolatedModel{ii} = cellfun(@(x) sum(repmat(basis_expansion.',size(x,1), 1) .* x(:,:,nearest_points{ii}(1)),2), obj.model_coefficients, 'uni', false);
            case 2
                obj.InterpolatedModel{ii} = cellfun(@(x) sum(repmat(basis_expansion.', size(x,1),1) .* x(:,:,nearest_points{ii}(1), nearest_points{ii}(2)),2), obj.model_coefficients, 'uni', false);
        end
    end
end


function idx = findClosest(difference)
    [~, idx] = min(abs(difference));
end
